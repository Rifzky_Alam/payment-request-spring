-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Bulan Mei 2018 pada 04.37
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeke`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pr_items`
--

CREATE TABLE `pr_items` (
  `pri_id` int(8) NOT NULL,
  `pri_source` varchar(35) COLLATE utf8_bin NOT NULL,
  `pri_itm_name` varchar(75) CHARACTER SET utf8 NOT NULL,
  `pri_price` int(12) NOT NULL,
  `pri_qty` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `pr_items`
--

INSERT INTO `pr_items` (`pri_id`, `pri_source`, `pri_itm_name`, `pri_price`, `pri_qty`) VALUES
(1, '1', 'Aquarium', 600000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pr_lists`
--

CREATE TABLE `pr_lists` (
  `prl_id` int(8) NOT NULL,
  `prl_requestor` varchar(75) COLLATE utf8_bin NOT NULL,
  `prl_status` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `pr_lists`
--

INSERT INTO `pr_lists` (`prl_id`, `prl_requestor`, `prl_status`) VALUES
(1, 'ikan', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_desc`
--

CREATE TABLE `status_desc` (
  `sd_id` int(9) NOT NULL,
  `sd_table` varchar(75) CHARACTER SET utf8 NOT NULL,
  `sd_source` varchar(35) COLLATE utf8_bin NOT NULL,
  `sd_desc` varchar(225) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `status_desc`
--

INSERT INTO `status_desc` (`sd_id`, `sd_table`, `sd_source`, `sd_desc`) VALUES
(1, 'pr_lists', '1', 'Client created a request');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `usr_username` varchar(35) COLLATE utf8_bin NOT NULL,
  `usr_password` varchar(35) COLLATE utf8_bin NOT NULL,
  `usr_nama` varchar(75) CHARACTER SET utf8 NOT NULL,
  `usr_email` varchar(125) COLLATE utf8_bin NOT NULL,
  `usr_role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`usr_username`, `usr_password`, `usr_nama`, `usr_email`, `usr_role`) VALUES
('zeke', 'oke', 'Rifzky Alam', 'rifzky.mail@gmail.com', 1),
('rifzky', '06ffb4fb5ac9678196aa76caf49c99cb', 'Rifzky Alam', 'rifzky.apple@gmail.com', 2),
('ikan', 'b7099590ed85c311c49083f5830ac4a9', 'Lulu Nazifa', 'lulu.fac07@gmail.com', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(2) NOT NULL,
  `role_ket` varchar(75) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`role_id`, `role_ket`) VALUES
(1, 'administrator'),
(2, 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pr_items`
--
ALTER TABLE `pr_items`
  ADD PRIMARY KEY (`pri_id`);

--
-- Indeks untuk tabel `pr_lists`
--
ALTER TABLE `pr_lists`
  ADD PRIMARY KEY (`prl_id`);

--
-- Indeks untuk tabel `status_desc`
--
ALTER TABLE `status_desc`
  ADD PRIMARY KEY (`sd_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pr_items`
--
ALTER TABLE `pr_items`
  MODIFY `pri_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pr_lists`
--
ALTER TABLE `pr_lists`
  MODIFY `prl_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `status_desc`
--
ALTER TABLE `status_desc`
  MODIFY `sd_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
