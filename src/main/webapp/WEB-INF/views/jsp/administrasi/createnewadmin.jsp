<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 

<!-- link bootstrap is required -->
<!-- <link href="/resources/core/css/bootstrap.min.css" rel="stylesheet" /> -->
  <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'>

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheader.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h3>Input New ADMIN</h3>
    </div>  
  </div>
</div>
 
<div class="container">

	<form action="" method="POST" accept-charset="utf-8">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>Role</label>
        <select name="roleadmin" class="form-control" required>
          <option value="" selected>-- Choose role of user --</option>
          <c:if test="${ not empty roles }">
            <c:forEach var="obj" items="${ roles }">
              <option value="${ obj.id }">${ obj.rolename }</option>
            </c:forEach>
          </c:if>
        </select>
      </div>
      <div class="form-group">
        <label>Name</label>
        <input type="text" name="nama" class="form-control" placeholder="Name of User" required>
      </div>
      <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control" placeholder="Username will be used when you log in to our system." required>
      </div>
      <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password for logging in to our system" required>
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Email of User" required>
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>
    </div>
  </div>
  </form>
  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<!-- link bootstrap.js and jquery -->
 <script type="text/javascript" src='https://fac-institute.com/js/jquery.js'></script>
 <script type="text/javascript" src='https://fac-institute.com/js/bootstrap.min.js'></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>