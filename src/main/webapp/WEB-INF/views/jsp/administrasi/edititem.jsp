<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 

<!-- link bootstrap is required -->
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container-fluid">
  <div class="jumbotron">
    <h1>Welcome ${ nama }!</h1>
    <p>
      This page is supposed to be dashboard of users for purchase request.
    </p>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-12">
      <form action="" method="post">
      <div class="form-group">
        <label>Item Name</label>
        <input type="text" name="namaitem" value="${ itemname }" class="form-control" placeholder="input the requested item here">
      </div>
      <div class="form-group">
        <label>Price per item</label>
        <input type="number" name="price" value="${ price }" class="form-control" placeholder="The price of item">
      </div>
      <div class="form-group">
        <label>Quantity</label>
        <input type="number" name="qty" class="form-control" value="${ qty }" placeholder="The price of item">
      </div>
      <div class="form-group">
        <label>Status</label>
        <select name="statusbarang" class="form-control">
          <c:choose>
            <c:when test="${flag=='0'}">
              <option value="0" selected>Item For Comparation</option>
              <option value="1">Item to Purchase</option>
            </c:when>
            <c:otherwise>
              <option value="0">Item For Comparation</option>
              <option value="1" selected>Item to Purchase</option>
            </c:otherwise>
          </c:choose>
          
        </select>
      </div>
      <div class="form-group">
        <label>Notes</label>
        <textarea class="form-control" name="keteranganbarang">${ note }</textarea>
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<!-- link bootstrap.js and jquery -->
 <script type="text/javascript" src='https://fac-institute.com/js/jquery.js'></script>
 <script type="text/javascript" src='https://fac-institute.com/js/bootstrap.min.js'></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>