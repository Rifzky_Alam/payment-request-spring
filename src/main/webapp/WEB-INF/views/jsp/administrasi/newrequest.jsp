<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Input New Request</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<spring:url value="/resources/datepicker/jquery-ui.min.css" var="datepickercss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="${datepickercss}">
</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheader.jsp"></c:import>
 
 <c:if test="${ not empty errorinput })">
 	<script>alert("${errorinput}");</script>
 </c:if>
 
<div class="container" style="padding-top:50px;">
	<div class="row">
    <div class="page-header">
      <h2>New Request Form</h2>
    </div>
  </div>
 	<br>


  <div class="row">
    <div class="col-md-12">
      <form action="" method="post" accept-charset="utf-8">
        <div class="form-group">
           <label>Due</label>
           <input type="text" name="tanggal" id="tgl" class="form-control" placeholder="Format: yyyy-mm-dd">
        </div>
        <div class="form-group">
           <label>Shop Name</label>
           <input type="text" name="namatoko" id="namatoko" class="form-control" placeholder="The name of shop in which you would like to buy items">
        </div>
        <div class="form-group">
           <label>Shop Contact</label>
           <input type="text" name="telepontoko" id="telptoko" class="form-control" placeholder="Available contact number of the shop">
        </div>
        <div class="form-group">
           <label>Notes</label>
           <textarea name="notes" class="form-control" placeholder="you may enter any notes that may be helpful for additional information"></textarea>
        </div>
         <button class="btn btn-lg btn-success">Submit</button>            
      </form>
    </div>
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div> <!-- end container -->
 
<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<spring:url value="/resources/datepicker/datepicker.js" var="datepickerJs" />
 
<!-- <script src="${coreJs}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${bootstrapJs}"></script>
<script src="${datepickerJs}"></script>

<script>
  $(document).ready(function(){
    $('#tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });
</script>
</body>
</html>