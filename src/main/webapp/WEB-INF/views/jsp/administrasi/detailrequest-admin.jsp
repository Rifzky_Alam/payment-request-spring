<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Purchase Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- link bootstrap is required -->
<link href="${bootstrapCss}" rel="stylesheet" />
  <!-- <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'> -->

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h2>Detail Request ${ request.labelPr }</h2>
    </div>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-6">
      <table class="table table-bordered">
        <tbody>
          <tr>
            <td colspan="2"><b>${ request.requestor }</b></td>
          </tr>
          <tr>
            <td>Due</td>
            <td>${ request.tgl }</td>
          </tr>
          <tr>
            <td>Status</td>
            <td>${ request.status }</td>
          </tr>
          <tr>
            <td>Request Created At</td>
            <td>${ request.itimestamp }</td>
          </tr>
          <tr>
            <td>Shop Name</td>
            <td>${ request.shopname }</td>
          </tr>
          <tr>
            <td>Shop Contact</td>
            <td>${ request.shopcontact }</td>
          </tr>
          <tr>
            <td>Notes</td>
            <td>${ request.notes }</td>
          </tr>
        </tbody>
      </table>
      <a href="/${ basepath }/editrequest/${ request.id }" class="btn btn-lg btn-warning">Edit</a>
    </div>
    <div class="col-md-6">
      <h4>Items to Purchase <a href="/${basepath}/newitem/${ request.id }" title=""><span class="btn btn-xs btn-success">+</span></a></h4> 
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Item Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
        <c:choose>
          <c:when test="${empty items}">
            <tr>
              <td colspan="4" style="text-align:center;">Items of The request are not available.</td>
            </tr>
          </c:when>
          <c:otherwise>
            <c:set var="sumtotal" value="${0}" />
            <c:forEach var="obj" items="${ items }">
              <c:set var="total" value="${ obj.price * obj.qty }" />
              <c:set var="sumtotal" value="${ sumtotal + total }" />
              <tr>
                <td>${ obj.itemname } <a href="/${ basepath }/edititem/${ obj.id }" class="btn btn-xs btn-warning">~></a> <a href="/${ basepath }/deleteitem/${ obj.id }" class="btn btn-xs btn-danger">X</a></td>
                <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="3" value="${ obj.price }"/> IDR</td>
                <td>${ obj.qty }</td>
                <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="3" value="${ total }"/> IDR</td>
              </tr>
            </c:forEach>
              <tr>
                <td><b>Total</b></td>
                <td colspan="3" style="text-align:right;"><b><fmt:formatNumber type="number" maxFractionDigits="3" value="${ sumtotal }"/> IDR</b></td>
              </tr>
          </c:otherwise>
        </c:choose>
        </tbody>
      </table>
      <br>
      <h4>Comparation Items</h4>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Item Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <c:choose>
            <c:when test="${empty itemkomparasi}">
              <tr>
                <td colspan="3" style="text-align:center;">Tidak Ada Data Tersedia</td>
              </tr>
            </c:when>
            <c:otherwise>
              <c:set var="sumtotal" value="${0}" />
              <c:forEach var="itm" items="${ itemkomparasi }">
                <c:set var="total" value="${ itm.price * itm.qty }" />
                <c:set var="sumtotal" value="${ sumtotal + total }" />
                <tr>
                  <td>${ itm.itemname } <a href="/${ basepath }/edititem/${ itm.id }" class="btn btn-xs btn-warning">~></a> <a href="/${ basepath }/deleteitem/${ itm.id }" class="btn btn-xs btn-danger">X</a></td>
                  <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="3" value="${ itm.price }"/> IDR</td>
                  <td>${itm.qty}</td>
                  <td>${ itm.keterangan }</td>
                </tr>
              </c:forEach>
              <tr>
                <td colspan="3"><b>Total</b></td>
                <td style="text-align:right;"><b><fmt:formatNumber type="number" maxFractionDigits="3" value="${ sumtotal }"/> IDR</b></td>
              </tr>
            </c:otherwise>
          </c:choose>
        </tbody>
      </table>
    </div>
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<!-- link bootstrap.js and jquery -->
 <script type="text/javascript" src='https://fac-institute.com/js/jquery.js'></script>
 <script type="text/javascript" src='https://fac-institute.com/js/bootstrap.min.js'></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>