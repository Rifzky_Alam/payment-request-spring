<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Purchase Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- link bootstrap is required -->
<link href="${bootstrapCss}" rel="stylesheet" />
  <!-- <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'> -->

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h2>Report Bug List</h2>
    </div>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST">
      <div class="form-group">
        <label>Report</label>
        <textarea name="reporterror" class="form-control">${ rb.report }</textarea>
      </div>
      <div class="form-group">
        <label>Link</label>
        <input type="text" name="linkerror" class="form-control" value="${rb.links}" placeholder="link (on addressbar) that error occurs">
      </div>
      <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
          <c:forEach var="obj" items="${ datastatus }">
          <c:choose>
            <c:when test="${ rb.status == obj.id }">
              <option value="${ obj.id }" selected="selected">${ obj.desc }</option>
            </c:when>
            <c:otherwise>
              <option value="${ obj.id }">${ obj.desc }</option>
            </c:otherwise>
          </c:choose>
          
          </c:forEach>
        </select>
      </div>
      <button class="btn btn-lg btn-warning">Edit</button>
      </form>
    </div>
    
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<!-- link bootstrap.js and jquery -->
 <script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>