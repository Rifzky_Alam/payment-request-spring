<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Purchase Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- link bootstrap is required -->
<link href="${bootstrapCss}" rel="stylesheet" />
  <!-- <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'> -->

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h2>Report Bug</h2>
    </div>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-12">
      <c:if test="${ totaldata==0 }">
        <a href="#" class="btn btn-info">New Email</a>
        <br><br>  
      </c:if>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Account</th>
            <th>Host</th>
            <th>Port</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <c:choose>
            <c:when test="${ totaldata==0 }">
              <tr>
                <td style="text-align:center;" colspan="4">Belum ada data tersedia</td>
              </tr>    
            </c:when>
            <c:otherwise>
              <c:forEach var="obj" items="${ dataemail }">
                <tr>
                  <td>${ obj.account }</td>
                  <td>${ obj.hostname }</td>
                  <td>${ obj.portnumber }</td>
                  <td><a href="/${ basepath }/admin/editmailconfig">Edit</a></td>
                </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
          
        </tbody>
      </table>
    </div>
    
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<!-- link bootstrap.js and jquery -->
 <script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>