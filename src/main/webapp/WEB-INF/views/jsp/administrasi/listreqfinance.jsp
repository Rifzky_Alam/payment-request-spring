<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 

<!-- link bootstrap is required -->
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<spring:url value="/resources/datepicker/jquery-ui.min.css" var="datepickerCss" />
<!-- <link href="/resources/core/css/bootstrap.min.css" rel="stylesheet" /> -->
  <link href="${bootstrapCss}" rel="stylesheet" />
  <link href="${datepickerCss}" rel="stylesheet" />
</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container-fluid">
  <div class="jumbotron">
    <h1>Welcome ${ nama }</h1>
    <p>
      This page is supposed to be dashboard of Admin for purchase request.
    </p>
  </div>
</div>
 
<div class="container">
	
  <div class="row" style="text-align:right;">
        <div class="col-md-12">
            <a data-toggle="modal" href="#modal-cari" class="btn btn-danger">Search</a>
        </div>
    </div><br>


  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Req No</th>
            <th>Requestor</th>
            <th>Items</th>
            <th>Total</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <c:choose>
              <c:when test="${empty datatable}">
                  <tr>
                    <td colspan="4" style="text-align:center;">Tidak Ada Data dalam database kami</td>
                  </tr>
              </c:when>
              <c:otherwise>
                  <c:forEach var="obj" items="${ datatable }">
                    <c:if test = "${ obj.flag == '-1' }">
                      <tr class="danger">
                    </c:if>
                    <c:if test = "${ obj.flag == '1' }">
                      <tr>
                    </c:if>
                    <c:if test = "${ obj.flag == '2' }">
                      <tr class="warning">
                    </c:if>
                    <c:if test = "${ obj.flag == '3' }">
                      <tr class="success">
                    </c:if>
                    <c:if test = "${ obj.flag == '4' }">
                      <tr class="info">
                    </c:if>
                      <td>${ obj.labelPr }</td>
                      <td>${ obj.requestor }</td>
                      <td>
                        <ul>
                        <c:forEach var="obj2" items="${ obj.items }">
                          <li>${ obj2.itemname }</li>
                        </c:forEach>
                        </ul>
                      </td>
                      <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="3" value="${ obj.totalvalues }" /> IDR</td>
                      <td>
                        <ul>
                            <c:choose>
                              <c:when test="${empty obj.adminappv}">
                                <li style="color:red;">
                                Approved By Admin (X)    
                              </c:when>
                              <c:otherwise>
                                <li>
                                Approved By Admin (${ obj.adminappv})
                              </c:otherwise>
                            </c:choose>
                          </li>
                            <c:choose>
                              <c:when test="${empty obj.financeappv}">
                                <li style="color:red;">
                                Approved By Finance (X)    
                              </c:when>
                              <c:otherwise>
                                <li>
                                Approved By Finance (${ obj.financeappv})
                              </c:otherwise>
                            </c:choose>
                          </li>
                          
                            <c:choose>
                              <c:when test="${empty obj.directorappv}">
                                <li style="color:red;">
                                Approved By Director (X)    
                              </c:when>
                              <c:otherwise>
                                <li>
                                Approved By Director (${ obj.directorappv})
                              </c:otherwise>
                            </c:choose>
                          </li>
                        </ul>
                      </td>
                      <td>
                        <a href="/${ basepath }/request/${ obj.id }/detail" title="Detail">Detail</a>
                        ||
                        <a href="/${ basepath }/request/${ obj.id }/approve" title="Approve">Approve</a>
                        ||
                        <a target="_blank" href="/${ basepath }/${ obj.id }/showpdf" title="PDF">Get PDF</a>
                        ||
                        <a href="/${ basepath }/request/${ obj.id }/reject" title="Reject">Reject</a>
                      </td>
                    </tr>
                  </c:forEach>
              </c:otherwise>
          </c:choose>
        </tbody>
      </table>
      <!-- start pagination -->
      <div class="row">
        <div class="col-md-12">
          <center>
            <c:choose>
              <c:when test="${ page != 0 }">
                <c:choose>
                  <c:when test="${ page * limit < totaldata}">
                    Showing data ${ (page*limit)-limit } - ${ page * limit } rows of ${ totaldata }
                  </c:when>
                  <c:otherwise>
                    Showing data ${ (page*limit)-limit } - ${ totaldata } rows of ${ totaldata }
                  </c:otherwise>
                </c:choose>  
              </c:when>
              <c:otherwise>
                <c:choose>
                  <c:when test="${ totaldata <= limit }">
                    Showing data 0 - ${ totaldata } rows of ${ totaldata }
                  </c:when>
                  <c:otherwise>
                    Showing data 0 - ${ limit } rows of ${ totaldata }
                  </c:otherwise>
                </c:choose>
              </c:otherwise>
            </c:choose>
                               
          </center>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">
            <c:choose>
              <c:when test="${ page == 0 }">
                <c:set var="blok" value="0" />
              </c:when>
              <c:otherwise>
                <c:set var="blok" value="${ page/limitlist }" />
              </c:otherwise>
            </c:choose>
            <c:if test="${ blok>0 }">
              <fmt:formatNumber var="j" value="${ page/limitlist }" maxFractionDigits="0" />
              
              <li><a href="/${ basepath }/listrequest/${ j }" title="">< Prev</a></li>
            </c:if>
            
            <c:forEach begin="${ blok * limitlist }" end="${ blok * limitlist + 5 }" varStatus="loop">
              <c:if test="${ loop.index * limit < totaldata }">
                <li><a href="/${ basepath }/listrequest/${ loop.index + 1 }" title="${ loop.index + 1 }">${ loop.index + 1 }</a></li>  
                <c:set var="ai" value="${ loop.index }" />
              </c:if>
            </c:forEach>
            <c:if test="${ totaldata > limit * limitlist }">
              <c:if test="${ blok < totaldata/limitlist && ai*limit<totaldata }">
                <c:set var="jei" value="${ blok * limitlist + 6 }" />
                <li><a href="/${ basepath }/listrequest/${ jei }" title=""> Next ></a></li>
              </c:if>
            </c:if>
            </ul>
        </center>
        </div>
      </div>
      <!-- end pagination -->
    </div>
  </div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Searching Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="/${ basepath }/tessearch/" method="get">
            <div class="row">
              <div class="form-group">
                  <label for="shopname">Shop Name</label>
                  <input id="shopname" type="text" name="shopname" class="form-control" placeholder="Shop the requestor would like to buy">
              </div>

              <div class="form-group">
                  <label for="requestorz">Requestor</label>
                  <select id="requestorz" name="requestorz" class="form-control">
                    <option value="" selected="selected">-- choose user --</option>
                    <c:forEach var="obj3" items="${ datausers }">
                      <option value="${ obj3.username }">${ obj3.name }</option>
                    </c:forEach>
                  </select>
              </div>

              <div class="form-group">
                  <label for="itemname">Item Name</label>
                  <input id="itemname" type="text" name="itemname" class="form-control" placeholder="Item the requestor wants to buy">
              </div>

              <div class="form-group">
                  <label for="fromdate">From Date</label>
                  <input id="fromdate" type="text" name="fromdate" class="form-control" placeholder="Due date from">
              </div>

              <div class="form-group">
                  <label for="todate">To Date</label>
                  <input id="todate" type="text" name="todate" class="form-control" placeholder="Due date until">
              </div>

              <div class="form-group">
                  <label for="statuspr">Status PR</label>
                  <select name="statuspr" id="statuspr" class="form-control">
                    <option value="" selected="selected">-- choose status --</option>
                    <c:forEach var="obj4" items="${ dataprlist }">
                      <option value="${ obj4.id }">${ obj4.desc }</option>
                    </c:forEach>
                  </select>
              </div>

            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Search</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>PR - IntelliSys 2018</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 
<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<spring:url value="/resources/datepicker/datepicker.js" var="datepickerJs" />
<!-- link bootstrap.js and jquery -->
 <script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${datepickerJs}"></script>
<script>
  $(document).ready(function(){
    $('#fromdate').datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $('#todate').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });
</script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>