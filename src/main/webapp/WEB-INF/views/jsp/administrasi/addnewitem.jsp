<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 

<!-- link bootstrap is required -->
<!-- <link href="/resources/core/css/bootstrap.min.css" rel="stylesheet" /> -->
  <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'>

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h2>New Item For Request ${ request.labelPr }</h2>  
    </div>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-12">
      <form action="" method="post">
      <div class="form-group">
        <label>Item Name</label>
        <input type="text" name="namaitem" value="" class="form-control" placeholder="input the requested item here">
      </div>
      <div class="form-group">
        <label>Price per item</label>
        <input type="number" name="price" class="form-control" placeholder="The price of item">
      </div>
      <div class="form-group">
        <label>Quantity</label>
        <input type="number" name="qty" class="form-control" placeholder="The price of item">
      </div>
      <div class="form-group">
        <label>Status</label>
        <select name="statusbarang" class="form-control">
          <option value="1" selected>Purchase item</option>
          <option value="0">Item for comparation</option>
        </select>
      </div>
      <div class="form-group">
        <label>Notes</label>
        <textarea class="form-control" name="keteranganbarang"></textarea>
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<!-- link bootstrap.js and jquery -->
 <script type="text/javascript" src='https://fac-institute.com/js/jquery.js'></script>
 <script type="text/javascript" src='https://fac-institute.com/js/bootstrap.min.js'></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>