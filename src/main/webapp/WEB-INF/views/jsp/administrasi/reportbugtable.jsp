<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Purchase Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- link bootstrap is required -->
<link href="${bootstrapCss}" rel="stylesheet" />
  <!-- <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'> -->

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h2>Report Bug List</h2>
    </div>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>User</th>
            <th>Error</th>
            <th>Link</th>
            <th>Status</th>
            <th>Timestamp</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="obj" items="${ datatable }">
            <tr>
              <td>${obj.username}</td>
              <td>${obj.report}</td>
              <td>${obj.links}</td>
              <td>${obj.statusDesc}</td>
              <td>${obj.stempelwaktu}</td>
              <td><a href="/${ basepath }/editreportbug/${ obj.id }" title="change status/edit">Change</a></td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
    
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<!-- link bootstrap.js and jquery -->
 <script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>