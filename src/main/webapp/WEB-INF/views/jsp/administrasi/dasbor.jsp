<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
<style type="text/css" media="screen">
  .tengah{
    text-align:center;
  }
</style>
</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheader.jsp"></c:import>
 
<div class="container-fluid">
  <div class="jumbotron">
    <h1>Welcome ${nama}!</h1>
    <p>
    </p>
  </div>
</div>
 
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<a href="/${ basepath }/newrequest" class="btn btn-primary">Create new request</a>
		</div>
	</div>
 	<br>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
      <thead>
      	<tr>
          <th class="tengah">PR No</th>
      		<th class="tengah">Requester</th>
      		<th class="tengah">Items</th>
      		<th class="tengah">Total</th>
      		<th class="tengah">Status</th>
      		<th class="tengah">Actions</th>
      	</tr>
      </thead>
      <tbody>
      	<c:forEach var="obj" items="${ datatable }">
          <c:if test = "${ obj.flag == '-1' }">
            <tr class="danger">
          </c:if>
          <c:if test = "${ obj.flag == '1' }">
            <tr>
          </c:if>
          <c:if test = "${ obj.flag == '2' }">
            <tr class="warning">
          </c:if>
          <c:if test = "${ obj.flag == '3' }">
            <tr class="success">
          </c:if>
          <c:if test = "${ obj.flag == '4' }">
            <tr class="info">
          </c:if>
          <td>${ obj.labelPr }</td>
      		<td>${ obj.requestor }</td>
      		<td>
      			<ul>
      				<c:forEach var="obj2" items="${ obj.items }">
      					<li>${ obj2.itemname }</li>
      				</c:forEach>
      			</ul>
      		</td>
      		<td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="3" value="${ obj.totalvalues }" /> IDR</td>
      		<td>${ obj.status }</td>
      		<td>
            <a href="/${ basepath }/request/${ obj.id }/detail">Edit</a>
            ||
            <a href="/${ basepath }/newitem/${ obj.id }">New Item</a>
            ||
            <a target="_blank" href="/${ basepath }/${ obj.id }/showpdf">Get PDF</a>
            ||
            <a href="/${ basepath }/request/${obj.id}/delete" title="Edit">Remove</a>
          </td>
      	</tr>
      	</c:forEach>
      </tbody>
      </table>   
    </div>
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 
<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
 
</body>
</html>