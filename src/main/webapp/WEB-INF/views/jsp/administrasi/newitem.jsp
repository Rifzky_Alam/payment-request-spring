<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheader.jsp"></c:import>
 
<div class="container-fluid">
  <div class="jumbotron">
    <h1>New Item on Request Payment!</h1>
    <p>
      Add more items when you need it!.
    </p>
  </div>
</div>
 
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <section>
          <form action="/request/newitem" method="post" accept-charset="utf-8">
              <div class="form-group">
                <label>Nama Item</label>
                <input type="text" name="namaitem" value="" placeholder="Type your item name here!" class="form-control">
              </div>
              <div class="form-group">
                <label>Qty</label>
                <input type="text" name="jumlah" value="" placeholder="Quantity of item (only number)" class="form-control">
              </div>
              <div class="form-group">
                <label>Unit Price</label>
                <input type="text" name="harga" value="" placeholder="Price per pcs" class="form-control">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name="statusbarang" class="form-control">
                  <option value="1" selected>Purchase item</option>
                  <option value="0">Item for comparation</option>
                </select>
              </div>
              <div class="form-group">
                <label>Notes</label>
                <textarea class="form-control" name="keteranganbarang"></textarea>
              </div>
              <button title="Submit" class="btn btn-lg btn-danger">Submit</button>
          </form>
          
          
        </p>
      </section>
    </div>
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 
<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
 
</body>
</html>