
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
  <spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
  <link rel="stylesheet" href="">
</head>
<body>
    
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>

<div class="container" style="padding-bottom:50px;">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h2>Payment Request - Admin</h2>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
            <thead>
              <tr>
                <th>Requester</th>
                <th>Items</th>
                <th>Total</th>
                <th>Due</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="5">Belum Ada data di database kami</td>
              </tr>
            </tbody>
      </table>  
    </div>      
  </div>
</div>
<spring:url value="/resources/core/css/hello.js" var="coreJs" />
<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
<script src=""></script>
<script src=""></script>

</body>
</html>