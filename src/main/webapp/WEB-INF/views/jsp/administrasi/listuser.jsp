<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Payment Request | Dashboard</title>
 

<!-- link bootstrap is required -->
<!-- <link href="/resources/core/css/bootstrap.min.css" rel="stylesheet" /> -->
  <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'>

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h3>List of Users</h3>
    </div>  
  </div>
</div>
 
<div class="container">

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <c:choose>
            <c:when test="${empty datatable}">
              <tr>
                <td colspan="4" style="text-align:center">Data is not available</td>
              </tr>
            </c:when>
            <c:otherwise>
              <c:forEach var="obj" items="${ datatable }">
                <tr>
                  <td>${ obj.name }</td>
                  <td>${ obj.username }</td>
                  <td>${ obj.email }</td>
                  <td>${ obj.irole }</td>
                  <td><a href="/${ basepath }/admin/edituser/${obj.username}" title="Edit">Edit</a></td>
                </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
        </tbody>
      </table>
    </div>
  </div>
  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<!-- link bootstrap.js and jquery -->
 <script type="text/javascript" src='https://fac-institute.com/js/jquery.js'></script>
 <script type="text/javascript" src='https://fac-institute.com/js/bootstrap.min.js'></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>