<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Purchase Request | Dashboard</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- link bootstrap is required -->
<link href="${bootstrapCss}" rel="stylesheet" />
  <!-- <link rel="stylesheet" type="text/css" href='https://fac-institute.com/css/bootstrap.css'> -->

</head>
 
<c:import url="/WEB-INF/views/jsp/administrasi/topnavheaderadmin.jsp"></c:import>
 
<div class="container">
  <div class="row">
    <div class="page-header">
      <h2>Email For Sytem</h2>
    </div>
  </div>
</div>
 
<div class="container">
	
  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST" accept-charset="utf-8">
        <div class="form-group">
            <label for="wassup">Account Name</label>
            <textarea id="wassup" class="form-control" name="account" placeholder="type the error here"></textarea>
        </div>
        <div class="form-group">
            <label for="linkz">Password</label>
            <input type="password" name="password" class="form-control" placeholder="link the error occurrs">
        </div>
        <div class="form-group">
            <label for="host">Host</label>
            <input type="text" id="host" name="hostname" class="form-control" placeholder="link the error occurrs">
        </div>
        <div class="form-group">
            <label for="port">Port Number</label>
            <input type="text" id="port" name="portnumber" class="form-control" placeholder="link the error occurrs">
        </div>
        <button class="btn btn-lg btn-danger">Submit</button>        
      </form>
    </div>
    
  </div>

  
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
 

<spring:url value="/resources/core/js/jquery.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<!-- link bootstrap.js and jquery -->
 <script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<!-- <script src="/resources/core/js/jquery.js"></script> -->
<!-- <script src="/resources/core/js/bootstrap.min.js"></script> -->
 
</body>
</html>