<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Login Form</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>
 
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">zeke - PR IntelliSys</a>
	</div>
  </div>
</nav>


 
<div class="container-fluid" style="padding-top:70px;">
  <div class="jumbotron">
    <h1>Welcome To Payment Request System!</h1>
    <p>
      This page is created for registration of user who is willing to be a member of PR System in PT IntelliSys.
    </p>
  </div>
</div>
  


<div class="container">
 
  <div class="row">
    <div class="col-md-12">
      
    </div>
  </div>

  <div class="row">
  	<div class="col-md-12">
  		<form name='loginForm' action="/${basepath}/registrasi" method="POST">
      <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control">  
      </div>
      <div class="form-group">
        <label>Password</label>
        <input class="form-control" type="password" name="password">  
      </div>
      <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" class="form-control">
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control">
      </div>
      <br>
      <button class="btn btn-lg btn-primary">Login</button>
      </form>
  	</div>
  </div>
  
  <hr>
  <footer>
	<p>&copy; zeke 2018</p>
  </footer>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<spring:url value="/resources/core/css/hello.js" var="coreJs" />
<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <c:if test="${not empty errmsg}">
    <script type="text/javascript">
    $(document).ready(function(){
      alert('Database Error, cannot input new data.');
    });
  </script>
</c:if>
</body>
</html>