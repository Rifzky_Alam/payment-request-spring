<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Login Form</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>
 
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">zeke - PR IntelliSys</a>
	</div>
  </div>
</nav>
 
<div class="container" style="padding-top:40px;">
  <div class="row">
    <div class="page-header">
      <h1>Welcome To Purchase Request System!</h1>
    </div>
  </div>
</div>
 
<div class="container">
 
  <div class="row">
    <div class="col-md-12">
      
    </div>
  </div>

  <div class="row">
  	<div class="col-md-12">
  		<form name='loginForm' action="/${basepath}/login" method="POST">
      <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control">  
      </div>
      <div class="form-group">
        <label>Password</label>
        <input class="form-control" type="password" name="password">  
      </div>
      <c:if test="${invalid eq 'y'}">
      <span style="color:red;">Invalid username and password!</span>
      <br>
      </c:if>
      <br>
      <button class="btn btn-lg btn-primary">Login</button>
      <a href="/${ basepath }/registrasi" class="btn btn-lg btn-info">I Don't have an account yet</a>
      </form>
  	</div>
  </div>
  
  <hr>
  <footer>
	<p>&copy; PT Intellisys 2018</p>
  </footer>
</div>
 
<spring:url value="/resources/core/css/hello.js" var="coreJs" />
<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 
</body>
</html>