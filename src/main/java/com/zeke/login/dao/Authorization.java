package com.zeke.login.dao;
import com.zeke.beans.User;
import com.zeke.security.MyHash;
public class Authorization extends MySqlDbase{
	
	public User login(User user) {
		try {
			MyHash ihash = new MyHash();
			this.Connect();
			String query = "SELECT `usr_username`, `usr_password`, `usr_nama`, `usr_email`, `usr_role` "
					+ "FROM user "
					+ "WHERE usr_username=? AND usr_password=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, user.getUsername());
			stmt.setString(2, ihash.emdelima(user.getPassword()));
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				user.setUsername(rs.getString("usr_username"));
				user.setPassword(rs.getString("usr_password"));
				user.setName(rs.getString("usr_nama"));
				user.setEmail(rs.getString("usr_email"));
				user.setIrole(rs.getString("usr_role"));
			}
			
			return user;
		} catch (Exception e) {
			System.out.println(e.toString());
			return new User("","","","","");
		}
	}
	
	public boolean Registrasi(User user) {
		try {
			MyHash ihash = new MyHash();
			this.Connect();
			String query;
			query = "INSERT INTO user (`usr_username`, `usr_password`, `usr_nama`, `usr_email`, `usr_role`)"
					+ "VALUES(?,?,?,?,?)";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, user.getUsername());
			stmt.setString(2, ihash.emdelima(user.getPassword()));
			stmt.setString(3, user.getName());
			stmt.setString(4, user.getEmail());
			stmt.setString(5, "2");
			stmt.executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}
	}
	
}
