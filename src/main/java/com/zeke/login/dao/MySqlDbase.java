package com.zeke.login.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MySqlDbase {
	protected String JDBC_Driver = "com.mysql.jdbc.Driver";
    protected String DB_URL="jdbc:mysql://localhost:3306/paymentrequest";
    protected String user = "root";
    protected Connection conn;
    protected PreparedStatement stmt;
    protected ResultSet rs;
    
    protected void Connect() {
		try {
            Class.forName(JDBC_Driver).newInstance();
            conn = DriverManager.getConnection(DB_URL,"root","");
        } catch (Exception e) {
            System.out.println("Error in connection: " + e.toString());
        }
	}
    
    protected void closeConn() {
    	try {
			conn.close();
		} catch (Exception e) {
			System.out.println("Error when closing db connection: "+ e.toString());
		}
    }
}
