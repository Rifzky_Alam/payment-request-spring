package com.zeke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zeke.beans.User;
import com.zeke.login.dao.Authorization;

@Controller
public class AuthController {
	private String invalid;
	private String routehome = "pr-1.0";
//	private String routehome = "";
	
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ModelAndView Login(HttpSession session) {
		if(session.getAttribute("username")!=null) {
			if(session.getAttribute("role").toString().equals("2")) {
				return new ModelAndView("redirect:/dasbor");
			}else {
				return new ModelAndView("redirect:/dasboradmin");
			}
		}
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("invalid", this.invalid);
		mav.addObject("basepath",this.routehome);
		mav.setViewName("login/login");
		return mav;
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView loginpost(HttpServletRequest req, HttpSession session) {
		User user = new User(req.getParameter("username"),req.getParameter("password"));
		Authorization auth = new Authorization();
		user = auth.login(user);
//		System.out.println(user.getName());
		//view setup
		if(user.getName()==null) {
			this.invalid = "y";
			return new ModelAndView("redirect:/");
		}else {
			this.invalid = "n";
			session.setAttribute("username", user.getUsername());
			session.setAttribute("nama", user.getName());
			session.setAttribute("email", user.getEmail());
			session.setAttribute("role", user.getIrole());
			
			/*
			ModelAndView mav = new ModelAndView();
			mav.addObject("nama", user.getName());
			mav.addObject("username", user.getUsername());
			mav.addObject("password", user.getPassword());
			mav.addObject("myrole", user.getIrole());
			mav.setViewName("administrasi/dasbor");
			*/
			if (user.getIrole().equals("2")) {
				return new ModelAndView("redirect:/dasbor");
			} else {
				return new ModelAndView("redirect:/dasboradmin");
			}
			
		}

	}
	
	@RequestMapping(value="/registrasi", method=RequestMethod.GET)
	public ModelAndView registrasi() {
		ModelAndView model = new ModelAndView();
		model.addObject("basepath",this.routehome);
		model.setViewName("login/registrasi");
		return model;
	}
	
	@RequestMapping(value="/registrasi", method=RequestMethod.POST)
	public ModelAndView postregistrasi(HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		Authorization auth = new Authorization();
		User user = new User();
		//setting bean values
		user.setUsername(req.getParameter("username"));
		user.setPassword(req.getParameter("password"));
		user.setEmail(req.getParameter("email"));
		user.setName(req.getParameter("nama"));
		
		if (auth.Registrasi(user)) {
			return new ModelAndView("redirect:/");
		} else {
			model.addObject("errmsg", "0");
			model.addObject("basepath",this.routehome);
			model.setViewName("login/registrasi");
			return model;
		}
		
	}
	
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {
		session.removeAttribute("username");
		session.removeAttribute("nama");
		session.removeAttribute("email");
		session.removeAttribute("role");
		return new ModelAndView("redirect:/");
	}
	
	
	
}
