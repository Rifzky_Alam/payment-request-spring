package com.zeke.web.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zeke.beans.Emailbean;
import com.zeke.beans.PaymentRequest;
import com.zeke.beans.Paymentrequeststatus;
import com.zeke.beans.PrItem;
import com.zeke.beans.Reportbug;
import com.zeke.beans.Reportbugstatus;
import com.zeke.beans.User;
import com.zeke.beans.Userroles;
import com.zeke.email.Apacheemail;
import com.zeke.functions.StrFunc;
import com.zeke.pr.dao.Admindao;
import com.zeke.pr.dao.Clientdao;
import com.zeke.pr.dao.Emaildao;
import com.zeke.pr.dao.Reportdao;
import com.zeke.reporting.Laporanpdf;

@Controller
public class AdminController {
	// disable autowired, cannot have more than 1 autowired
	private String routehome = "pr-1.0";
//	private String routehome = "";
	
	
	
	
	//input reportbug
	@RequestMapping(value="/reportbug", method=RequestMethod.GET)
	public ModelAndView reportbug(HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
				
		ModelAndView mav = new ModelAndView();
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.addObject("basepath",this.routehome);
		mav.setViewName("administrasi/reportbug");
		return mav;
	}
	
	@RequestMapping(value="/reportbug", method=RequestMethod.POST)
	public ModelAndView reportbugpost(HttpServletRequest req, HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		Reportbug report = new Reportbug();
		Reportdao db = new Reportdao();
		
		report.setUsername(session.getAttribute("username").toString());
		report.setReport(req.getParameter("errorreport"));
		report.setLinks(req.getParameter("linkerror"));
		
		if (db.inputReport(report)) {
			return new ModelAndView("redirect:/dasboradmin");
		} else {
			return new ModelAndView("redirect:/reportbug");
		}
	}
	
	//edit reportbug
	@RequestMapping(value="/editreportbug/{id:.+}", method=RequestMethod.GET)
	public ModelAndView editreportbug(@PathVariable("id") String id,HttpSession session){
		ModelAndView mav = new ModelAndView();
		Reportdao db = new Reportdao();
		
		ArrayList<Reportbugstatus> datastatus = db.getStatusreportlist();
		
		Reportbug rb = db.getSingleRow(id);
		// common variables
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		
		// data
		mav.addObject("datastatus", datastatus);
		mav.addObject("rb", rb);
		
		mav.setViewName("administrasi/editreportbug");
		return mav;
	}
	
	@RequestMapping(value="/editreportbug/{id:.+}", method=RequestMethod.POST)
	public ModelAndView editrequestpost(@PathVariable("id") String id,HttpServletRequest req, HttpSession session) {
		Reportdao db = new Reportdao();
		Reportbug rb = new Reportbug();
		rb.setId(id);
		rb.setReport(req.getParameter("reporterror"));
		rb.setLinks(req.getParameter("linkerror"));
		rb.setStatus(Integer.parseInt(req.getParameter("status")));
		
		if (db.updateReportBug(rb)) {
			return new ModelAndView("redirect:/reportbuglists");
		} else {
			return new ModelAndView("redirect:/editreportbug/"+id);
		}
		
		
	}
	
	
	// reportbug tables
	@RequestMapping(value="/reportbuglists", method= RequestMethod.GET)
	public ModelAndView reportbugtable(HttpSession session) {
		Reportdao db = new Reportdao();
		ModelAndView mav = new ModelAndView();
		
		ArrayList<Reportbug> datatable = db.listtable();
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("datatable", datatable);
		mav.addObject("basepath",this.routehome);
		mav.setViewName("administrasi/reportbugtable");
		return mav;
	}
	
	@RequestMapping(value="/dasboradmin", method=RequestMethod.GET)
	public ModelAndView dasboradmin(HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		ArrayList<PaymentRequest> pr = new ArrayList<PaymentRequest>();
		ArrayList<User> userlist = new ArrayList<User>();
		ArrayList<Paymentrequeststatus> prstatlist;
		Admindao admindata = new Admindao();
		
		pr = admindata.tabeldasbor();
		userlist = admindata.listUsers();
		prstatlist = admindata.fetchPrstatus();
		
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.addObject("basepath",this.routehome);
		mav.addObject("datatable",pr);
		mav.addObject("datausers", userlist);
		mav.addObject("dataprlist", prstatlist);
		mav.addObject("page", 0);
		mav.addObject("limit", 30);
		mav.addObject("limitlist", 5);
		mav.addObject("totaldata", pr.size());
		mav.setViewName("administrasi/listreqfinance");
		
		return mav;
	}
	
	@RequestMapping(value="/listrequest/{page:.+}", method=RequestMethod.GET)
	public ModelAndView listrequest(@PathVariable("page") String page,HttpSession session) {
		int intpage = Integer.parseInt(page) * 30;
		// authenticate user
				if(session.getAttribute("username")==null) {
					return new ModelAndView("redirect:/");
				}else if(session.getAttribute("role").toString().equals("2")){
					return new ModelAndView("redirect:/dasbor");
				}
				
				ArrayList<PaymentRequest> pr = new ArrayList<PaymentRequest>();
				Admindao admindata = new Admindao();
				
				pr = admindata.tabeldasbor(String.valueOf(intpage),"30");
				
				ModelAndView mav = new ModelAndView();
				
				mav.addObject("nama", session.getAttribute("nama"));
				mav.addObject("username", session.getAttribute("username"));
				mav.addObject("email", session.getAttribute("email"));
				mav.addObject("myrole", session.getAttribute("role").toString());
				mav.addObject("basepath",this.routehome);
				mav.addObject("datatable",pr);
				mav.addObject("page", Integer.parseInt(page));
				mav.addObject("limit", 30);
				mav.addObject("limitlist", 5);
				mav.addObject("totaldata", pr.size());
				mav.setViewName("administrasi/listreqfinance");
				
				return mav;
	}
	
	@RequestMapping(value="/tessearch", method = RequestMethod.GET)
	public ModelAndView searchpaymentrequest(HttpServletRequest req, HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		ModelAndView mav = new ModelAndView();
		Admindao db = new Admindao();
		PaymentRequest paymentrequest = new PaymentRequest();
		paymentrequest.setShopname(StrFunc.cekNull(req.getParameter("shopname")));
		paymentrequest.setRequestor(StrFunc.cekNull(req.getParameter("requestorz")));
		paymentrequest.setItimestamp(StrFunc.cekNull(req.getParameter("fromdate")));
		paymentrequest.setTgl(StrFunc.cekNull(req.getParameter("todate")));
		paymentrequest.setSingleitem(StrFunc.cekNull(req.getParameter("itemname")));
		paymentrequest.setStatus(StrFunc.cekNull(req.getParameter("statuspr")));
		
		ArrayList<PaymentRequest> pr;
		ArrayList<User> userlist;
		ArrayList<Paymentrequeststatus> prstatlist;
		
		pr = db.tabelsearchpaymentrequest(paymentrequest);
		userlist = db.listUsers();
		prstatlist = db.fetchPrstatus();
		
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.addObject("basepath",this.routehome);
		mav.addObject("datatable",pr);
		mav.addObject("datausers", userlist);
		mav.addObject("dataprlist", prstatlist);
		mav.addObject("page", 0);
		mav.addObject("limit", 30);
		mav.addObject("limitlist", 5);
		mav.addObject("totaldata", pr.size());
		mav.setViewName("administrasi/listreqfinance");
		
//		System.out.println("requestor: "+StrFunc.cekNull(req.getParameter("reqname")));
//		System.out.println("shopname: "+StrFunc.cekNull(req.getParameter("shopname")));
//		System.out.println("date from: "+StrFunc.cekNull(req.getParameter("fromdate")));
//		System.out.println("date to: "+StrFunc.cekNull(req.getParameter("todate")));
//		System.out.println("itemname: "+StrFunc.cekNull(req.getParameter("itemname")));
//		System.out.println("statuspr: "+StrFunc.cekNull(req.getParameter("statuspr")));
		
		return mav;
	}
	
	@RequestMapping(value="/dasbor", method=RequestMethod.GET)
	public ModelAndView dasbor(HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		ArrayList<PaymentRequest> pr = new ArrayList<PaymentRequest>();
		Clientdao clientdata = new Clientdao();
		
		ModelAndView mav = new ModelAndView();
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		pr = clientdata.tabeldasbor(String.valueOf(session.getAttribute("username")));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		if (session.getAttribute("role").toString().equals("2")) {
			mav.addObject("datatable",pr);
			if (pr == null) {
				System.out.println("PR is empty");
			}
		}else {
			return new ModelAndView("redirect:/dasboradmin");
		}
		mav.setViewName("administrasi/dasbor");
		
		return mav;
	}
	
	@RequestMapping(value="/{id:.+}/showpdf", method=RequestMethod.GET)
	public String showpdf(@PathVariable("id") String id,HttpServletResponse response, 
			HttpServletRequest request,HttpSession session) {
//		ModelAndView mav = new ModelAndView();
		if(session.getAttribute("username")==null) {
			return null;
		}
		
		try {
			Laporanpdf laporan = new Laporanpdf();
			
			String pathfile = request.getSession().getServletContext().getRealPath("/") + "tes.pdf";
			File file = new File(pathfile);
			
			if (file.exists()) {
				file.delete();
			}
			
			laporan.createPdf(id, pathfile);
			
			Path path = Paths.get(pathfile);
			byte [] documentInBytes = Files.readAllBytes(path);
			response.setHeader("Content-Disposition", "inline; filename=\"report.pdf\"");
			response.setDateHeader("Expires", -1);
	        response.setContentType("application/pdf");
	        response.setContentLength(documentInBytes.length);
	        response.getOutputStream().write(documentInBytes);
		} catch (Exception e) {
			System.out.println("Error on controller: "+ e.toString());
			return null;
		}
		
		return null;
	}
	
	@RequestMapping(value="/admin/newemailforsystem", method = RequestMethod.POST)
	public ModelAndView newemailforsystempost(HttpSession session, HttpServletRequest req) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		Emaildao db = new Emaildao();
		Emailbean bean = new Emailbean();
		
		bean.setAccount(req.getParameter("account"));
		bean.setPassword(req.getParameter("password"));
		bean.setHostname(req.getParameter("hostname"));
		bean.setPortnumber(Integer.parseInt(req.getParameter("portnumber")));
		
		if (db.insertnewEmail(bean)) {
			return new ModelAndView("redirect:/admin/emailconfigdata");
		} else {
			return new ModelAndView("redirect:/");
		}
	}
	
	@RequestMapping(value="/admin/newemailforsystem", method = RequestMethod.GET)
	public ModelAndView newemailforsystem(HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		ModelAndView mav = new ModelAndView();
		Emaildao db = new Emaildao();
		ArrayList<Emailbean> beans = db.getLists();
		
		if (beans.size()==0) { // beans.size==0
			mav.addObject("username", session.getAttribute("username"));
			mav.addObject("nama", session.getAttribute("nama"));
			mav.addObject("email", session.getAttribute("email"));
			mav.addObject("myrole", session.getAttribute("role"));
			mav.addObject("basepath",this.routehome);
			
			mav.setViewName("administrasi/newemailforsystem");
			return mav;
		} else {
			return new ModelAndView("redirect:/");
		}
		
		
	}
	
	@RequestMapping(value="/admin/editmailconfig", method = RequestMethod.POST)
	public ModelAndView posteditEmailconfig(HttpSession session, HttpServletRequest req) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		Emaildao db = new Emaildao();
		Emailbean bean = new Emailbean();
		
		bean.setAccount(req.getParameter("account"));
		bean.setPassword(req.getParameter("password"));
		bean.setHostname(req.getParameter("hostname"));
		bean.setPortnumber(Integer.parseInt(req.getParameter("portnumber")));
		
		// checking data
		System.out.println("Account: " + req.getParameter("account"));
		System.out.println("Password: " + req.getParameter("password"));
		System.out.println("Hostname: " + req.getParameter("hostname"));
		System.out.println("Port Num: " + req.getParameter("portnumber"));
		
		
		if (db.updateDataEmail(bean)) {
			return new ModelAndView("redirect:/admin/emailconfigdata");
		} else {
			return new ModelAndView("redirect:/admin/editmailconfig");
		}
		
	}
	
	@RequestMapping(value="/admin/editmailconfig", method = RequestMethod.GET)
	public ModelAndView editemailconfig(HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		ModelAndView mav = new ModelAndView();
		Emaildao db = new Emaildao();
		Emailbean bean = db.getConfigValues();
		
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		
		mav.addObject("bean", bean);
		
		mav.setViewName("administrasi/editemailconfig");
		return mav;
		
	}
	
	@RequestMapping(value="/admin/emailconfigdata", method = RequestMethod.GET)
	public ModelAndView listregisteredemailforsystem(HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		ModelAndView mav = new ModelAndView();
		Emaildao db = new Emaildao();
		ArrayList<Emailbean> beans = db.getLists();
		
		mav.addObject("dataemail", beans);
		mav.addObject("totaldata", beans.size());
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		
		mav.setViewName("administrasi/listemailforsystem");
		return mav;
	}
	
	@RequestMapping(value="/{id:.+}/emailrejection", method = RequestMethod.GET)
	public ModelAndView emailRejectionnotif(@PathVariable("id") String id,HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		PaymentRequest pr = new PaymentRequest();
		pr.setId(id);
		
		String msg = "Purchase Request anda ("+pr.getLabelPr()+") telah ditolak.";
		
		Admindao admindao = new Admindao();
		User user = new User();
		user = admindao.sendingEmaildata(id);
		
		Apacheemail email = new Apacheemail();
		
		email.sendEmail("Pemberitahuan PR ("+pr.getLabelPr()+") PT IntelliSys tidak diterima",msg,user.getEmail());
		
		return new ModelAndView("redirect:/dasboradmin");
	}
	
	@RequestMapping(value="/{id:.+}/tesemail",method=RequestMethod.GET)
	public ModelAndView emailnotif(@PathVariable("id") String id, HttpSession session) {
		// authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		PaymentRequest pr = new PaymentRequest();
		pr.setId(id);
		
		String msg = "Anda telah menerima persetujuan atas PR " + pr.getLabelPr() + " di purchase request system PT Intellisys";
		String role = "";
		if (session.getAttribute("role").toString().equals("1")) {
			msg += " oleh Admin";
			role = "(Admin)";
		}else if(session.getAttribute("role").toString().equals("3")) {
			msg += " oleh Finance";
			role = "(Finance)";
		}else if(session.getAttribute("role").toString().equals("4")) {
			msg += " oleh Director";
			role = "(Director)";
		}
		
		
		Admindao admindao = new Admindao();
		User user = new User();
		user = admindao.sendingEmaildata(id);
		
		
		Apacheemail email = new Apacheemail();
		
		email.sendEmail("Pemberitahuan Approval PR ("+pr.getLabelPr()+") PT IntelliSys "+ role,msg,user.getEmail());
//		laporan.createPdf("1","files/tes.pdf");
		
		return new ModelAndView("redirect:/dasboradmin");
	}
	
	@RequestMapping(value="/admin/edituser/{id:.+}", method=RequestMethod.GET)
	public ModelAndView edituser(@PathVariable("id") String id, HttpSession session) {
		ModelAndView mav = new ModelAndView();
		Admindao db = new Admindao();
		User user = db.getSingledatauser(id);
		user.setUserstatus(db.fetchUserstatuses());
		user.setRolelists(db.fetchUserRoles());
		mav.addObject("userdata", user);
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		
		mav.setViewName("administrasi/editusers");
		return mav;
	}
	
	@RequestMapping(value="/admin/edituser/{id:.+}", method=RequestMethod.POST)
	public ModelAndView edituserpost(@PathVariable("id")String idusername, HttpSession session, HttpServletRequest req) {
		User user = new User();
		
		user.setUsername(idusername);
		user.setName(req.getParameter("nama"));
		user.setEmail(req.getParameter("email"));
		user.setIrole(req.getParameter("role"));
		user.setStatus(req.getParameter("status"));
		
		Admindao db = new Admindao();
		
		if (db.updateUser(user)) {
			return new ModelAndView("redirect:/dasboradmin");
		} else {
			return new ModelAndView("redirect:/admin/edituser/"+idusername);
		}
		
	}
	
	@RequestMapping(value="/admin/listusers", method=RequestMethod.GET)
	public ModelAndView listusers(HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		
		ModelAndView mav = new ModelAndView();
		Admindao db = new Admindao();
		
		ArrayList<User> users = db.listUsers();
		
		mav.addObject("datatable", users);
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		mav.setViewName("administrasi/listuser");
		return mav;
	}
	
	@RequestMapping(value="/admin/newadmin", method=RequestMethod.POST)
	public ModelAndView postadminbaru(HttpSession session, HttpServletRequest req) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		Admindao db = new Admindao();
		
		User newuser = new User();
		newuser.setUsername(req.getParameter("username"));
		newuser.setPassword(req.getParameter("password"));
		newuser.setName(req.getParameter("nama"));
		newuser.setEmail(req.getParameter("email"));
		newuser.setIrole(req.getParameter("roleadmin"));
		
		if (db.inputNewuser(newuser)) {
			return new ModelAndView("redirect:/admin/listusers");
		} else {
			return new ModelAndView("redirect:/admin/newadmin");
		}
	}
	
	@RequestMapping(value="/admin/newadmin", method=RequestMethod.GET)
	public ModelAndView adminbaru(HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")){
			return new ModelAndView("redirect:/dasbor");
		}
		
		Admindao db = new Admindao();
		ModelAndView mav = new ModelAndView();
		
		ArrayList <Userroles> someroles = db.getAllRoles();
		
		mav.addObject("roles", someroles);
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		mav.setViewName("administrasi/createnewadmin");
		return mav;
	}
	
	@RequestMapping(value="/request/{id:.+}/detail",method=RequestMethod.GET)
	public ModelAndView detailrequest(@PathVariable("id") String id, HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null||session.getAttribute("username").toString().equals("")) {
			return new ModelAndView("redirect:/");
		}
		
		ModelAndView mav = new ModelAndView();
		Admindao dao = new Admindao();
		
		PaymentRequest pr = dao.getPaymentrequestbyid(id);
		ArrayList<PrItem> items = dao.getItemrequest(pr);
		ArrayList<PrItem> itemkomparasi = dao.getItemComparation(pr);
		mav.addObject("request", pr);
		mav.addObject("items", items);
		mav.addObject("itemkomparasi",itemkomparasi);
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("email", session.getAttribute("email"));
		mav.addObject("myrole", session.getAttribute("role"));
		mav.addObject("basepath",this.routehome);
		mav.setViewName("administrasi/detailrequest-admin");
		return mav;
	}
	
	@RequestMapping(value="/request/{id:.+}/approve",method=RequestMethod.GET)
	public ModelAndView approverequest(@PathVariable("id") String id, HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null||session.getAttribute("username").toString().equals("")) {
			return new ModelAndView("redirect:/");
		}else if(session.getAttribute("role").toString().equals("2")) {
			return new ModelAndView("redirect:/");
		}
		
		
		
//		ModelAndView mav = new ModelAndView();
		Admindao dao = new Admindao();
		
		PaymentRequest pr = dao.getPaymentrequestbyid(id);
		int hasRow = dao.cekApproval(session.getAttribute("role").toString(), id);
		if (session.getAttribute("role").toString().equals("1")) {
			pr.setFlag(2);
		}else if(session.getAttribute("role").toString().equals("3")) {
			pr.setFlag(3);
		}else if(session.getAttribute("role").toString().equals("4")){
			pr.setFlag(4);
		}
		
		if (hasRow==0) {
			if(dao.insertApprovalusername(session.getAttribute("username").toString(), id)) {
				if (dao.updateRequestflag(pr)) {
					System.out.println("Info: Request "+ id + " has been updated!");
					return new ModelAndView("redirect:/"+id+"/tesemail");
				} else {
					System.out.println("Info: Request "+ id + " failed to be updated!");
					return new ModelAndView("redirect:/dasboradmin");
				}
			}else {
				System.out.println("Error inserting Approval Username");
				return new ModelAndView("redirect:/dasboradmin");
			}
		}else {
			return new ModelAndView("redirect:/dasboradmin");
		}
		
		
//		mav.addObject("obj", pr);
//		mav.setViewName("administrasi/tes");
		
//		return mav;
	}
	
	@RequestMapping(value="/request/{id:.+}/reject",method=RequestMethod.GET)
	public ModelAndView rejectrequest(@PathVariable("id") String id, HttpSession session) {
		Admindao dao = new Admindao();
		PaymentRequest pr = dao.getPaymentrequestbyid(id);
		pr.setFlag(-1);
		if (dao.updateRequestflag(pr)) {
			return new ModelAndView("redirect:/"+ id +"/emailrejection");
		} else {
			return new ModelAndView("redirect:/dasboradmin");
		}
	}
	
	@RequestMapping(value="/editrequest/{id:.+}", method=RequestMethod.GET)
	public ModelAndView editrequestz(HttpSession session, @PathVariable("id") String id) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		ModelAndView mav = new ModelAndView();
		Admindao db = new Admindao();
		PaymentRequest pr = db.getSingleRequest(id);
		mav.addObject("tanggal", pr.getTgl());
		mav.addObject("shopname", pr.getShopname());
		mav.addObject("shopcontact", pr.getShopcontact());
		mav.addObject("notes", pr.getNotes());
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.setViewName("administrasi/editrequest");
		return mav;
	}
	
	@RequestMapping(value="/editrequest/{id:.+}", method=RequestMethod.POST)
	public ModelAndView posteditrequest(HttpSession session, @PathVariable("id") String id, HttpServletRequest req) {
		PaymentRequest pr = new PaymentRequest();
		pr.setId(id);
		pr.setShopname(req.getParameter("namatoko"));
		pr.setShopcontact(req.getParameter("telepontoko"));
		pr.setNotes(req.getParameter("notes"));
		pr.setTgl(req.getParameter("tanggal"));
		
		Admindao db = new Admindao();
		if (db.editRequest(pr)) {
			return new ModelAndView("redirect:/dasbor");
		} else {
			return new ModelAndView("redirect:/editrequest/"+id);
		}
		
		
	}
	
	
	@RequestMapping(value="/newrequest", method=RequestMethod.GET)
	public ModelAndView createnewrequest(HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("basepath",this.routehome);
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.setViewName("administrasi/newrequest");
		
		return mav;
	}
	
	@RequestMapping(value="/newrequest", method=RequestMethod.POST)
	public ModelAndView createnewrequestpost(HttpServletRequest req,HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		// getting request param
		PaymentRequest pr = new PaymentRequest();
		pr.setRequestor(session.getAttribute("username").toString());
		pr.setTgl(req.getParameter("tanggal"));
		pr.setShopname(req.getParameter("namatoko"));
		pr.setShopcontact(req.getParameter("telepontoko"));
		
		// call dao
		Clientdao cldb = new Clientdao();
		if (cldb.inputRequestbaru(pr)==1) {
			return new ModelAndView("redirect:/dasbor");
		} else {
			ModelAndView mav = new ModelAndView();
			mav.addObject("basepath",this.routehome);
			mav.addObject("errorinput","Kesalahan ketika memasukkan data, silahkan coba lagi!");
			mav.setViewName("administrasi/newrequest");
			return mav;
		}
	}
	
	//edit item from purchase to comparation
	@RequestMapping(value="/edititem/{id:.+}", method=RequestMethod.GET)
	public ModelAndView edititemrequest(@PathVariable("id") String id, HttpSession session) {
		ModelAndView mav = new ModelAndView();
		Admindao db = new Admindao();
		
		PrItem item = db.getItemdata(id);
		mav.addObject("itemname", item.getItemname());
		mav.addObject("price", String.valueOf(item.getPrice()));
		mav.addObject("qty", String.valueOf(item.getQty()));
		mav.addObject("flag", String.valueOf(item.getStatus()));
		mav.addObject("note", item.getKeterangan());
		mav.addObject("myrole", session.getAttribute("role").toString());
		
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.addObject("basepath", this.routehome);
		mav.setViewName("administrasi/edititem");
		return mav;
	}
	
	@RequestMapping(value="/edititem/{id:.+}",method=RequestMethod.POST)
	public ModelAndView postedititemrequest(@PathVariable("id") String id, HttpSession session,HttpServletRequest req) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		Clientdao db = new Clientdao();
		PrItem item = new PrItem();
		item.setId(Integer.parseInt(id));
		item.setItemname(req.getParameter("namaitem"));
		item.setKeterangan(req.getParameter("keteranganbarang"));
		item.setPrice(Integer.parseInt(req.getParameter("price")));
		item.setQty(Integer.parseInt(req.getParameter("qty")));
		item.setStatus(Integer.parseInt(req.getParameter("statusbarang")));
		
		if (db.editItemrequest(item)) {
			System.out.println("INFO: Successfully updated!!");
			if (session.getAttribute("role").toString().equals("2")) {
				return new ModelAndView("redirect:/dasbor");
			} else {
				return new ModelAndView("redirect:/dasboradmin");
			}
		} else {
			System.out.println("Error: Data failed to update!!");
			return new ModelAndView("redirect:/edititem/"+id);
		}
		
//		System.out.println("Item Name: " + req.getParameter("namaitem"));
//		System.out.println("Price: " + req.getParameter("price"));
//		System.out.println("Quantity: " + req.getParameter("qty"));
//		System.out.println("Status: " + req.getParameter("statusbarang"));
//		System.out.println("Notes: " + req.getParameter("keteranganbarang"));
		
		
	}
	
	@RequestMapping(value="/deleteitem/{id:.+}", method=RequestMethod.GET)
	public ModelAndView deleteitemrequest(@PathVariable("id") String id, HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		PrItem item = new PrItem();
		item.setId(Integer.parseInt(id));
		Clientdao dao = new Clientdao();
		if(dao.deleteItemrequest(item)) {
			if (session.getAttribute("role").toString().equals("2")) {
				return new ModelAndView("redirect:/dasbor");
			} else {
				return new ModelAndView("redirect:/dasboradmin");
			}
		}else {
			System.out.println("INFO: Route redirect to dashboard!");
			if (session.getAttribute("role").toString().equals("2")) {
				return new ModelAndView("redirect:/dasbor");
			} else {
				return new ModelAndView("redirect:/dasboradmin");
			}
		}
		
	}
	
	@RequestMapping(value="/newitem/{id:.+}", method=RequestMethod.POST)
	public ModelAndView itemrequestpost(@PathVariable("id") String id, HttpServletRequest req, HttpSession session) {
		PrItem newitem = new PrItem();
		newitem.setItemname(req.getParameter("namaitem"));
		newitem.setSrc(id);
		newitem.setPrice(Integer.parseInt(req.getParameter("price")));
		newitem.setQty(Integer.parseInt(req.getParameter("qty")));
		newitem.setStatus(Integer.parseInt(req.getParameter("statusbarang")));
		newitem.setKeterangan(req.getParameter("keteranganbarang"));
		Clientdao dao = new Clientdao();
		if (dao.inputItemBaru(newitem)==1) {
			return new ModelAndView("redirect:/dasbor");
		} else {
			return new ModelAndView("redirect:/newitem/"+id);
		}
		
//		return new ModelAndView("redirect:https://fac-institute.com/");
	}
	
	@RequestMapping(value="/newitem/{id:.+}", method=RequestMethod.GET)
	public ModelAndView newitemrequest(@PathVariable("id") String id, HttpSession session) {
		//authenticate user
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		Admindao db = new Admindao();
		PaymentRequest pr = db.getSingleRequest(id);
		ModelAndView mav = new ModelAndView();
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("request", pr);
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.addObject("basepath",this.routehome);
		mav.setViewName("administrasi/addnewitem");
		return mav;
	}
	
	@RequestMapping(value="/request/delete", method=RequestMethod.POST)
	public ModelAndView postdeletePrdata(HttpServletRequest req,HttpSession session) {
		//declare bean
		PaymentRequest pr = new PaymentRequest();
		pr.setId(req.getParameter("idlist"));
		
		//declare dao
		Clientdao cd = new Clientdao();
		if (cd.deleterow(pr)) {
			return new ModelAndView("redirect:/dasbor");
		} else {
			return new ModelAndView("redirect:/dasbor");
		}
	}
	
	@RequestMapping(value="/request/{id:.+}/delete", method=RequestMethod.GET)
	public ModelAndView deletePrdata(@PathVariable("id") String id, HttpSession session) {
		//authenticate user		
		if(session.getAttribute("username")==null) {
			return new ModelAndView("redirect:/");
		}
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("idofrow", id);
		
		mav.addObject("username", session.getAttribute("username"));
		mav.addObject("nama", session.getAttribute("nama"));
		mav.addObject("myrole", session.getAttribute("role").toString());
		mav.addObject("basepath",this.routehome);
		
		mav.setViewName("administrasi/deleteprrow");
		return mav;
	}
}
