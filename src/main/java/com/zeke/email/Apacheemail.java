package com.zeke.email;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;

import com.zeke.beans.Emailbean;
import com.zeke.pr.dao.Emaildao;

public class Apacheemail {
	public void sendEmail(String subjek,String pesan, String emailTujuan) {
		try {
			Emaildao db = new Emaildao();
			Emailbean emailBean = db.getConfigValues();
			Email email = new SimpleEmail();
			email.setHostName(emailBean.getHostname());
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator(emailBean.getAccount(), emailBean.getPassword()));
			email.setSSLOnConnect(true);
			email.setFrom(emailBean.getAccount());
			email.setSubject(subjek);
			email.setMsg(pesan);
			email.addTo(emailTujuan);
			email.send();
		} catch (Exception e) {
			System.out.println("Error Sending Email: "+ e.toString());
		}
	}
}
