package com.zeke.reporting;

import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
//import java.util.Locale;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.zeke.beans.PaymentRequest;
import com.zeke.beans.PrItem;
import com.zeke.pr.dao.Admindao;

public class Laporanpdf {
	
	private PdfPTable approvalsignature(PaymentRequest pr) {
		try {
			PdfPTable table = new PdfPTable(3);
			Font fb = FontFactory.getFont(FontFactory.TIMES_ROMAN,9,Font.BOLD);
			table.setTotalWidth(new float[] {150,150,150});
			PdfPCell cell = new PdfPCell();
			table.setLockedWidth(true);
			float tinggi = 20f;
			float tinggi2 = 50f;
			
			// first row
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Aknowledged by Admin",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Approved by Accounting",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Approved by Director",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);
			
			//second row
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi2);
			cell.setPhrase(new Phrase(pr.getAdminappv(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi2);
			cell.setPhrase(new Phrase(pr.getFinanceappv(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi2);
			cell.setPhrase(new Phrase(pr.getDirectorappv(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
			table.addCell(cell);
			
			return table;
		} catch (Exception e) {
			System.out.println("Error in approvalsignature func: "+ e.toString());
			return null;
		}
	}
	
	private PdfPTable footerTitletable(PaymentRequest pr, ArrayList<PrItem> items) {
		try {
			PdfPTable table = new PdfPTable(6);
			table.setTotalWidth(new float[] {80,10,200,80,10,100});
			PdfPCell cell = new PdfPCell();
			table.setLockedWidth(true);
			float tinggi = 20f;
			Font fb = FontFactory.getFont(FontFactory.TIMES_ROMAN,9,Font.BOLD);
			//first row
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Process Date",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(":",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(pr.getTgl(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Due Date",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(":",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(pr.getTgl(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			//second row
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Shop Name",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(":",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(pr.getShopname(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Total Purchase",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(":",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			// calculation
			int totalprice = 0;
			for (PrItem prItem : items) {
				totalprice += prItem.getPrice() * prItem.getQty();
			}
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			//third row
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("Shop Contact",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(":",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase(pr.getShopcontact(),fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("",fb));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggi);
			cell.setPhrase(new Phrase("",fb));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			
			
			
			return table;
		} catch (Exception e) {
			System.out.println("error in footer table: "+ e.toString());
			return null;
		}
	}
	
	private PdfPTable signature(String user) {
		try {
			PdfPTable table = new PdfPTable(2);
			table.setTotalWidth(new float[] {225,225});
			PdfPCell cell = new PdfPCell();
			table.setLockedWidth(true);
			Float tinggijudul = 30f;
			Float tinggiIsi = 60f;
			//first row here
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggijudul);
			cell.setPhrase(new Phrase("Issued by,"));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell(new Phrase("Received By,"));
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggijudul);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			// second row here
			Font fu = FontFactory.getFont(FontFactory.TIMES_ROMAN,12,Font.UNDERLINE);
			cell = new PdfPCell(new Phrase(user,fu));
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggiIsi);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
			table.addCell(cell);
			
			cell = new PdfPCell(new Phrase("Finance Staff",fu));
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setFixedHeight(tinggiIsi);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
			table.addCell(cell);
			
			
			return table;
		} catch (Exception e) {
			System.out.println("Error in signature function: "+ e.toString());
			return null;
		}
	}
	
	private PdfPTable tabelku(ArrayList<PrItem> items) {
		try {
			// add new table
			PdfPTable table = new PdfPTable(5);
			table.setTotalWidth(new float[] {30, 180, 120, 50, 120});
			table.setLockedWidth(true);
			Float tinggihead = 25f;
			Float tebelbariskepala = 2f;
			// first row
			PdfPCell cell = new PdfPCell( new Phrase("NO"));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			cell = new PdfPCell( new Phrase("ITEM"));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			cell = new PdfPCell( new Phrase("PRICE"));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			cell = new PdfPCell( new Phrase("QTY"));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			cell = new PdfPCell( new Phrase("SUBTOTAL"));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			NumberFormat format = NumberFormat.getInstance();
			int no = 0;
			float tebelborderitem = 1f;
			int totalprice = 0;
			for (PrItem prItem : items) {
				no++;
				// first row
				cell = new PdfPCell( new Phrase(Integer.toString(no)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(tinggihead);
				cell.setBorderColor(BaseColor.BLACK);
				cell.setBorderWidth(tebelborderitem);
				table.addCell(cell);
				
				cell = new PdfPCell( new Phrase(prItem.getItemname()));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(tinggihead);
				cell.setBorderColor(BaseColor.BLACK);
				cell.setBorderWidth(tebelborderitem);
				table.addCell(cell);
				
				cell = new PdfPCell( new Phrase(format.format(prItem.getPrice()) + " IDR"));
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(tinggihead);
				cell.setBorderColor(BaseColor.BLACK);
				cell.setBorderWidth(tebelborderitem);
				table.addCell(cell);
				
				cell = new PdfPCell( new Phrase(Integer.toString(prItem.getQty())));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(tinggihead);
				cell.setBorderColor(BaseColor.BLACK);
				cell.setBorderWidth(tebelborderitem);
				table.addCell(cell);
				
				cell = new PdfPCell( new Phrase(format.format(prItem.getPrice() * prItem.getQty()) + " IDR"));
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(tinggihead);
				cell.setBorderColor(BaseColor.BLACK);
				cell.setBorderWidth(tebelborderitem);
				table.addCell(cell);
				//calculating totalprice
				totalprice += prItem.getPrice() * prItem.getQty();
			}
			
			cell = new PdfPCell( new Phrase("TOTAL"));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setColspan(4);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			cell = new PdfPCell( new Phrase(format.format(totalprice)+ " IDR"));
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setFixedHeight(tinggihead);
			cell.setBorderColor(BaseColor.BLACK);
			cell.setBorderWidth(tebelbariskepala);
			table.addCell(cell);
			
			return table;
		} catch (Exception e) {
			System.out.println("Error in PDF Table");
			return null;
		}
	}

	public void createPdf(String prid,String filename) {
		try {
			Admindao dao = new Admindao();
			PaymentRequest pr = dao.getPaymentrequestbyid(prid);
			ArrayList<PrItem> items = dao.getItemrequest(pr);
			// create document (Step 1)
			Document document = new Document();
			// create instance (Step 2)
			PdfWriter.getInstance(document, new FileOutputStream(filename));
			// Open Document (
			document.open();
			// add new paragraph 
			String header1 = "PT IntelliSys";
			String header2 = "Kompleks Roxymas Blok C3 No.38, Jl. KH. Hasyim Ashari, RW.8, Cideng, Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta\nKode Pos: 10150\n";
			String para1 = "Dear "+ pr.getRequestor() +",";
			
			String para2 = "This is a system-generated document from Purchase Request of PT IntelliSys. The document can be a proof of a valid purchase request."
					+ " Your detail request is shown below.";
			
			String footpara = "If you have any questions or if we can further assist you in any way, please feel free to email me or call us at (62) 81279222250.";
			
			
			Font fontHeader = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD);
			Font font1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12);
			Font font2 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8);
			Font bu = FontFactory.getFont(FontFactory.TIMES_ROMAN,12,Font.UNDERLINE);
			
			document.add(new Paragraph(header1,fontHeader));
			document.add(new Paragraph(header2,font2));
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph(para1,font1));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph(para2,font1));
			
			document.add(Chunk.NEWLINE);
			
			PdfPTable table = this.tabelku(items);
			
			document.add(table);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("Status:",font2));
			document.add(Chunk.NEWLINE);
			
			// status here
			int totalApproval = dao.cektotalApproval(prid);
			if (totalApproval == 3) {
				document.add(new Paragraph("- PR has been Approved.",font2));
			} else {
				document.add(new Paragraph("- " + String.valueOf(3-totalApproval) + " more approval(s) needed.",font2));
			}
			
			
			
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph(footpara,font1));
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			table = this.signature(pr.getRequestor());
			document.add(table);

			// add separator to page before last table
			DottedLineSeparator separator = new DottedLineSeparator();
	        separator.setPercentage(59500f / 523f);
	        Chunk linebreak = new Chunk(separator);
	        
	        document.add(linebreak);
	        Paragraph paragrap = new Paragraph();
	        paragrap.add("ACCOUNTING  & ADMIN DEPARTMENT ONLY");
	        paragrap.setAlignment(Element.ALIGN_CENTER);
	        document.add(paragrap);
	        
	        document.add(Chunk.NEWLINE);
	        
	        table = this.footerTitletable(pr, items);
	        document.add(table);
	        
	        document.add(Chunk.NEWLINE);
	        
	        table = this.approvalsignature(pr);
	        document.add(table);
	        
			//close document (Last Step)
			document.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
