package com.zeke.pr.dao;

import java.util.ArrayList;

import com.zeke.beans.PaymentRequest;
import com.zeke.beans.Paymentrequeststatus;
import com.zeke.beans.PrItem;
import com.zeke.beans.Roleuser;
import com.zeke.beans.User;
import com.zeke.beans.Userroles;
import com.zeke.beans.Userstatus;
import com.zeke.login.dao.MySqlDbase;
import com.zeke.security.MyHash;

public class Admindao extends MySqlDbase{
	
	public boolean updateUser(User user) {
		try {
			this.Connect();
			String query = "UPDATE `user` "
					+ "SET `usr_nama`=?,`usr_email`=?,`usr_role`=?,`usr_status`=? "
					+ "WHERE `usr_username`=?";
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, user.getName());
			stmt.setString(2, user.getEmail());
			stmt.setString(3, user.getIrole());
			stmt.setString(4, user.getStatus());
			stmt.setString(5, user.getUsername());
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error on updateUser: "+ e.toString());
			return false;
		}
	}
	
	public ArrayList<Paymentrequeststatus> fetchPrstatus(){
		try {
			ArrayList<Paymentrequeststatus> prstatuslist = new ArrayList<Paymentrequeststatus>();
			this.Connect();
			String query = "SELECT `fpl_id`, `fpl_desc` FROM `flag_pr_list`";
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				prstatuslist.add(new Paymentrequeststatus(rs.getString("fpl_id"),rs.getString("fpl_desc")));
			}
			
			this.closeConn();
			return prstatuslist;
		} catch (Exception e) {
			System.out.println("Error on fetchPrstatus: " + e.toString());
			return null;
		}
	}
	
	public ArrayList<Roleuser> fetchUserRoles(){
		try {
			this.Connect();
			ArrayList<Roleuser> userRoles = new ArrayList<Roleuser>();
			String query = "SELECT `role_id`, `role_ket` FROM `user_role`";
			stmt = conn.prepareStatement(query);
			
			rs = stmt.executeQuery();
			while(rs.next()) {
				userRoles.add(new Roleuser(rs.getString("role_id"), rs.getString("role_ket")));
			}
			this.closeConn();
			return userRoles;
		} catch (Exception e) {
			System.out.println("error in fetchUserRole: " + e.toString());
			return null;
		}
	}
	
	public ArrayList<Userstatus> fetchUserstatuses() {
		try {
			this.Connect();
			ArrayList<Userstatus> userstats = new ArrayList<Userstatus>();
			String query = "SELECT `fus_id`, `fus_desc` FROM `flag_user_status`";
			stmt = conn.prepareStatement(query);
			
			rs = stmt.executeQuery();
			while(rs.next()) {
				userstats.add(new Userstatus(rs.getString("fus_id"),rs.getString("fus_desc")));
			}
			this.closeConn();
			return userstats;
		} catch (Exception e) {
			System.out.println("error in fetchUserstatuses: " + e.toString());
			return null;
		}
	}
	
	public User getSingledatauser(String username) {
		try {
			this.Connect();
			User user = new User();
			
			String query = "SELECT `usr_username`, `usr_password`, `usr_nama`, `usr_email`, `usr_role`, `usr_status` "
					+ "FROM `user` "
					+ "WHERE `usr_username`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, username);
			
			rs = stmt.executeQuery();
			while(rs.next()) {
				user.setUsername(rs.getString("usr_username"));
				user.setName(rs.getString("usr_nama"));
				user.setPassword(rs.getString("usr_password"));
				user.setEmail(rs.getString("usr_email"));
				user.setIrole(rs.getString("usr_role"));
				user.setStatus(rs.getString("usr_status"));
			}
			
			this.closeConn();
			return user;
		} catch (Exception e) {
			System.out.println("error in getSingledatauser: "+e.toString());
			return null;
		}
	}
	
	
	public boolean editRequest(PaymentRequest pr) {
		try {
			this.Connect();
			String query = "UPDATE `pr_lists` "
					+ "SET `prl_shop_name`=?,`prl_shop_contact`=?,`prl_notes`=?,`prl_due`=? "
					+ "WHERE `prl_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, pr.getShopname());
			stmt.setString(2, pr.getShopcontact());
			stmt.setString(3, pr.getNotes());
			stmt.setString(4, pr.getTgl());
			stmt.setString(5, pr.getId());
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error on editRequest: "+ e.toString());
			return false;
		}
	}
	
	public PaymentRequest getSingleRequest(String id) {
		try {
			this.Connect();
			PaymentRequest pr = new PaymentRequest();
			String query = "SELECT `prl_id`, `prl_requestor`, `prl_shop_name`, `prl_shop_contact`, `prl_due`, `prl_process_date`, `prl_created_at`, `prl_status`,`prl_notes` "
					+ "FROM `pr_lists` "
					+ "WHERE `prl_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, id);
			
			rs = stmt.executeQuery();
			while(rs.next()) {
				pr.setId(rs.getString("prl_id"));
				
				pr.setTgl(rs.getString("prl_due"));
				pr.setShopname(rs.getString("prl_shop_name"));
				pr.setShopcontact(rs.getString("prl_shop_contact"));
				pr.setNotes(rs.getString("prl_notes"));
			}
			
			
			this.closeConn();
			return pr;
		} catch (Exception e) {
			System.out.println("Error on getsinglerequest: " + e.toString());
			return null;
		}
	}
	
	
	public User sendingEmaildata(String idRequest) {
		try {
			User user = new User();
			this.Connect();
			String query = "SELECT usr_email \n" + 
					"FROM `pr_lists`\n" + 
					"INNER JOIN user \n" + 
					"ON user.usr_username=pr_lists.prl_requestor\n" + 
					"WHERE `prl_id`=?";
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, idRequest);
			
			rs = stmt.executeQuery();
			while(rs.next()) {
				user.setEmail(rs.getString("usr_email"));
			}
			return user;
		} catch (Exception e) {
			System.out.println("error in model sendingEmaildata: "+e.toString());
			return null;
		}
	}
	
	public int cektotalApproval(String idSource) {
		try {
			this.Connect();
			int jumlah = 0;
			String query = "SELECT COUNT(`pra_id`) AS `jumlah` FROM `pr_approval` WHERE `pra_source`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, idSource);
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				jumlah = rs.getInt("jumlah");
			}
			
			this.closeConn();
			return jumlah;
		} catch (Exception e) {
			System.out.println("error in cektotalApproval: " + e.toString());		
			return 0;
		}
	}
	
	public int cekApproval(String role, String reqId) {
		try {
			this.Connect();
			int hasil = -1;
			String query = "SELECT COUNT(`pra_id`) AS `jumlah` " + 
					"FROM `pr_approval` " + 
					"INNER JOIN `user` " + 
					"ON `user`.`usr_username`=`pr_approval`.`pra_username` " + 
					"WHERE `user`.`usr_role`=? AND `pr_approval`.`pra_source`=?";
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, role);
			stmt.setString(2, reqId);
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				hasil = Integer.parseInt(rs.getString("jumlah"));
			}
			
			this.closeConn();
			return hasil;
		} catch (Exception e) {
			return -1;
		}
	}
	
	public boolean insertApprovalusername(String username, String reqId) {
		try {
			this.Connect();
			String query = "INSERT INTO `pr_approval`(`pra_source`, `pra_username`) "
					+ "VALUES(?,?)";
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, reqId);
			stmt.setString(2, username);
			
			stmt.executeUpdate();
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("error in insertApprovalUsername: "+ e.toString());
			return false;
		}
	}
	
	public ArrayList<User> listUsers(){
		ArrayList<User> users = new ArrayList<User>();
		try {
			this.Connect();
			String query = "SELECT `usr_username`, `usr_password`, `usr_nama`, `usr_email`, `user_role`.`role_ket` AS `role` " + 
					"FROM `user` " + 
					"INNER JOIN `user_role` " + 
					"ON `user_role`.`role_id`=`user`.`usr_role`";
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				users.add(new User(rs.getString("usr_username"), rs.getString("usr_password"), rs.getString("usr_nama"), rs.getString("usr_email"), rs.getString("role")));
			}
			
			this.closeConn();
			return users;
		} catch (Exception e) {
			return null;
		}
	}
	
	public boolean inputNewuser(User user) {
		try {
			MyHash ihash = new MyHash();
			this.Connect();
			String query = "INSERT INTO `user`(`usr_username`, `usr_password`, `usr_nama`, `usr_email`, `usr_role`) "
					+ "VALUES(?,?,?,?,?)";
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, user.getUsername());
			stmt.setString(2, ihash.emdelima(user.getPassword()));
			stmt.setString(3, user.getName());
			stmt.setString(4, user.getEmail());
			stmt.setString(5, user.getIrole());
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("error in inputNewuser func: "+ e.toString());
			return false;
		}
	}
	
	
	public ArrayList<Userroles> getAllRoles(){
		ArrayList<Userroles> roles = new ArrayList<Userroles>();
		try {
			this.Connect();
			String query = "SELECT `role_id`, `role_ket` FROM `user_role`";
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				roles.add(new Userroles(rs.getString("role_id"), rs.getString("role_ket")));
			}
			
			return roles;
		} catch (Exception e) {
			System.out.println("Error in getAllRolesFunc: "+ e.toString());
			return null;
		}
	}
	
	// edit item
	public PrItem getItemdata(String id) {
		try {
			this.Connect();
			PrItem item = new PrItem();
			String query = "SELECT * FROM `pr_items` WHERE `pri_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, id);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				item.setId(Integer.parseInt(rs.getString("pri_id")));
				item.setItemname(rs.getString("pri_itm_name"));
				item.setPrice(rs.getInt("pri_price"));
				item.setQty(rs.getInt("pri_qty"));
				item.setStatus(rs.getInt("pri_status"));
				item.setKeterangan(rs.getString("pri_ket"));
			}
			
			this.closeConn();
			return item;
		} catch (Exception e) {
			System.out.println("Error on getItemdata: "+e.toString());
			return null;
		}
	}
	
	public ArrayList<PrItem> getItemComparation(PaymentRequest pr){
		ArrayList<PrItem> tabel = new ArrayList<PrItem>();
		try {
			String query = "SELECT `pri_id`, `pri_source`, `pri_itm_name`, `pri_price`, `pri_qty`,`pri_ket`, `created_at` FROM `pr_items` WHERE `pri_source`= ? AND `pri_status`='0'";
			PrItem item;
			stmt = conn.prepareStatement(query);
			stmt.setString(1, pr.getId());
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				item = new PrItem();
				item.setId(rs.getInt("pri_id"));
				item.setItemname(rs.getString("pri_itm_name"));
				item.setKeterangan(rs.getString("pri_ket"));
				item.setPrice(rs.getInt("pri_price"));
				item.setQty(rs.getInt("pri_qty"));
				tabel.add(item);
			}
			
			return tabel;
		} catch (Exception e) {
			System.out.println("Error in getItemComparation (dao): "+ e.toString());
			return null;
		}
	}
	
	public ArrayList<PrItem> getItemrequest(PaymentRequest pr){
		ArrayList<PrItem> tabel = new ArrayList<PrItem>();
		try {
			String query = "SELECT `pri_id`, `pri_source`, `pri_itm_name`, `pri_price`, `pri_qty`, `created_at` FROM `pr_items` WHERE `pri_source`=? AND `pri_status`='1'";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, pr.getId());
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				tabel.add(
						new PrItem(
						Integer.parseInt(rs.getString("pri_id")), 
						rs.getString("pri_source"), 
						rs.getString("pri_itm_name"), 
						Integer.parseInt(rs.getString("pri_price")), 
						Integer.parseInt(rs.getString("pri_qty"))
						));
			}
			
			return tabel;
		} catch (Exception e) {
			System.out.println("Error on getItemRequest: " + e.toString());
			e.printStackTrace();
			return null;
		}
		
		
		
	}
	
	public boolean updateRequestflag(PaymentRequest pr) {
		try {
			String query = "UPDATE `pr_lists` SET `prl_status` = ? WHERE `pr_lists`.`prl_id` = ?;";
			
			this.Connect();
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, Integer.toString(pr.getFlag()));
			stmt.setString(2, pr.getId());
			stmt.executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println("Admin Model Error Message: "+ e.toString());
			return false;
		}
	}
	
	public PaymentRequest getPaymentrequestbyid(String id) {
		try {
			this.Connect();
			
			String query = "SELECT `prl_id`, `prl_requestor`, `prl_shop_name`, `prl_shop_contact`, `usr_nama`, `prl_due`, `prl_created_at`, `prl_status`, `fpl_desc`, `prl_notes`, cariuser('1',`prl_id`) AS `admin`, cariuser('3',`prl_id`) AS `finance`, cariuser('4',`prl_id`) AS `director`" + 
					"					FROM `pr_lists` " + 
					"					INNER JOIN user " + 
					"					ON user.usr_username=pr_lists.prl_requestor " + 
					"					INNER JOIN flag_pr_list " + 
					"					ON flag_pr_list.fpl_id=pr_lists.prl_status " + 
					"					WHERE `prl_id`= ?";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, id);
//			System.out.println(query);
			rs = stmt.executeQuery();
			
			
			PaymentRequest hasil = new PaymentRequest();
			
			while(rs.next()) {
				hasil.setId(rs.getString("prl_id"));
				hasil.setRequestor(rs.getString("usr_nama"));
				hasil.setShopname(rs.getString("prl_shop_name"));
				hasil.setNotes(rs.getString("prl_notes"));
				hasil.setShopcontact(rs.getString("prl_shop_contact"));
				hasil.setAdminappv(rs.getString("admin"));
				hasil.setFinanceappv(rs.getString("finance"));
				hasil.setDirectorappv(rs.getString("director"));
				hasil.setFlag(Integer.parseInt(rs.getString("prl_status")));
				hasil.setStatus(rs.getString("fpl_desc"));
				hasil.setTgl(rs.getString("prl_due"));
				hasil.setItimestamp(rs.getString("prl_created_at"));
			}
			
			

//			hasil.setRequestor("zeke");
			
			return hasil;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error in Admin model getPRequestById: " + e.toString());
//			System.out.println(e.printStackTrace());
			return new PaymentRequest();
		}
	}
	
	
	public ArrayList<PaymentRequest> tabelsearchpaymentrequest(PaymentRequest pr){
		try {
			ArrayList<PaymentRequest> tabel = new ArrayList<PaymentRequest>();
			this.Connect();
			String query = "SELECT `prl_id`, `prl_requestor`,`fpl_desc`, `prl_status`,\n" + 
					"					 cariuser('1',`prl_id`) AS `admin`,\n" + 
					"					 cariuser('3',`prl_id`) AS `finance`,\n" + 
					"					 cariuser('4',`prl_id`) AS `director`, \n" + 
					"					(\n" + 
					"						SELECT GROUP_CONCAT(`pr_items`.`pri_itm_name` SEPARATOR '#')  \n" + 
					"					    FROM `pr_items`\n" + 
					"					    WHERE `pr_items`.`pri_source`=`prl_id` AND `pr_items`.`pri_status`='1' \n" + 
					"					) AS items, \n" + 
					"					(\n" + 
					"					SELECT SUM(`pri_price` * `pri_qty`) \n" + 
					"					FROM `pr_items` \n" + 
					"					WHERE `pr_items`.`pri_source`=`prl_id` AND `pr_items`.`pri_status`='1'\n" + 
					"					) AS `total` \n" + 
					"					FROM `pr_lists`  \n" + 
					"					INNER JOIN `flag_pr_list`  \n" + 
					"					ON `flag_pr_list`.`fpl_id` = `pr_lists`.`prl_status` \n" + 
					"                    WHERE `prl_shop_name` LIKE '%"+pr.getShopname()+"%'";
			if (!"".equals(pr.getRequestor()) || pr.getRequestor() == null) {
				query += " AND `prl_requestor` LIKE '"+ pr.getRequestor() +"'";
			}
			
			if (!"".equals(pr.getStatus()) || pr.getStatus() == null) {
				query += " AND `prl_status` LIKE '"+ pr.getStatus() +"'";
			}
			
			if ((!"".equals(pr.getItimestamp()) || pr.getItimestamp() == null)&&(!"".equals(pr.getTgl()) || pr.getTgl() == null)) {
				query += " AND `prl_created_at` BETWEEN '"+pr.getItimestamp()+" 00:00:00' AND '"+pr.getTgl()+" 23:59:59'";
			}
			
			query += " HAVING items LIKE '%"+pr.getSingleitem()+"%'\n" + 
					" ORDER BY `pr_lists`.`prl_created_at` DESC ";
//			System.out.println(query);
			stmt = conn.prepareStatement(query);
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				tabel.add(
						new PaymentRequest(
								rs.getString("prl_id"),
								rs.getString("prl_requestor"),
								PaymentRequest.getItemnames(rs.getString("items")),
								Integer.parseInt(PaymentRequest.checkTotal(rs.getString("total"))),
								rs.getString("admin"),
								rs.getString("finance"),
								rs.getString("director"),
								rs.getString("fpl_desc"),
								Integer.parseInt(rs.getString("prl_status"))
						));
			}
			
			
			this.closeConn();
			return tabel;
		} catch (Exception e) {
			System.out.println("error in searching model: "+ e.toString());
			return null;
		}
	}
	
	
	public ArrayList<PaymentRequest> tabeldasbor() {
		try {
			ArrayList<PaymentRequest> tabel = new ArrayList<PaymentRequest>();
			
			this.Connect();
			String query = "SELECT `prl_id`, `prl_requestor`,`fpl_desc`, `prl_status`,"
					+ "cariuser('1',`prl_id`) AS `admin`,"
					+ "cariuser('3',`prl_id`) AS `finance`,"
					+ "cariuser('4',`prl_id`) AS `director`," + 
					"(" + 
					"	SELECT GROUP_CONCAT(`pr_items`.`pri_itm_name` SEPARATOR '#') " + 
					"    FROM `pr_items`" + 
					"    WHERE `pr_items`.`pri_source`=`prl_id` AND `pr_items`.`pri_status`='1'" + 
					") AS items, "
					+ "("
					+ "SELECT SUM(`pri_price` * `pri_qty`) "
					+ "FROM `pr_items` "
					+ "WHERE `pr_items`.`pri_source`=`prl_id` AND `pr_items`.`pri_status`='1'"
					+ ") AS `total` " + 
					"FROM `pr_lists` " + 
					"INNER JOIN `flag_pr_list` " + 
					"ON `flag_pr_list`.`fpl_id` = `pr_lists`.`prl_status` ORDER BY `pr_lists`.`prl_created_at` DESC LIMIT 0, 30";
			stmt = conn.prepareStatement(query);

			rs = stmt.executeQuery();
			
			while(rs.next()) {
				tabel.add(
						new PaymentRequest(
								rs.getString("prl_id"),
								rs.getString("prl_requestor"),
								PaymentRequest.getItemnames(rs.getString("items")),
								Integer.parseInt(PaymentRequest.checkTotal(rs.getString("total"))),
								rs.getString("admin"),
								rs.getString("finance"),
								rs.getString("director"),
								rs.getString("fpl_desc"),
								Integer.parseInt(rs.getString("prl_status"))
						));
//				System.out.println(rs.getString("prl_id") + " "+rs.getString("prl_requestor"));
			}
			
			return tabel;
		} catch (Exception e) {
			System.out.println("Error database dasbor: " + e.toString());
			return null;
		}
	}
	
	public ArrayList<PaymentRequest> tabeldasbor(String offset, String limit) {
		try {
			ArrayList<PaymentRequest> tabel = new ArrayList<PaymentRequest>();
			
			this.Connect();
			String query = "SELECT `prl_id`, `prl_requestor`,`fpl_desc`, `prl_status`,"
					+ "cariuser('1',`prl_id`) AS `admin`,"
					+ "cariuser('3',`prl_id`) AS `finance`,"
					+ "cariuser('4',`prl_id`) AS `director`," + 
					"(" + 
					"	SELECT GROUP_CONCAT(`pr_items`.`pri_itm_name` SEPARATOR '#') " + 
					"    FROM `pr_items`" + 
					"    WHERE `pr_items`.`pri_source`=`prl_id`" + 
					") AS items, "
					+ "("
					+ "SELECT SUM(`pri_price` * `pri_qty`) "
					+ "FROM `pr_items` "
					+ "WHERE `pr_items`.`pri_source`=`prl_id`"
					+ ") AS `total` " + 
					"FROM `pr_lists` " + 
					"INNER JOIN `flag_pr_list` " + 
					"ON `flag_pr_list`.`fpl_id` = `pr_lists`.`prl_status` ORDER BY `pr_lists`.`prl_created_at` DESC LIMIT " + offset + "," + limit;
			stmt = conn.prepareStatement(query);

			rs = stmt.executeQuery();
			
			while(rs.next()) {
				tabel.add(
						new PaymentRequest(
								rs.getString("prl_id"),
								rs.getString("prl_requestor"),
								PaymentRequest.getItemnames(rs.getString("items")),
								Integer.parseInt(PaymentRequest.checkTotal(rs.getString("total"))),
								rs.getString("admin"),
								rs.getString("finance"),
								rs.getString("director"),
								rs.getString("fpl_desc"),
								Integer.parseInt(rs.getString("prl_status"))
						));
//				System.out.println(rs.getString("prl_id") + " "+rs.getString("prl_requestor"));
			}
			
			return tabel;
		} catch (Exception e) {
			System.out.println("Error database dasbor: " + e.toString());
			return null;
		}
	}
	
}
