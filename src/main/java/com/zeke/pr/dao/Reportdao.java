package com.zeke.pr.dao;

import java.util.ArrayList;

import com.zeke.beans.Reportbug;
import com.zeke.beans.Reportbugstatus;
import com.zeke.login.dao.MySqlDbase;

public class Reportdao extends MySqlDbase{
	
	public boolean updateReportBug(Reportbug rb) {
		try {
			this.Connect();
			String query = "UPDATE `report_bugs` "
					+ "SET `rb_report`=?, `rb_links`=?, `rb_status`=? "
					+ "WHERE `rb_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, rb.getReport());
			stmt.setString(2, rb.getLinks());
			stmt.setString(3, String.valueOf(rb.getStatus()));
			stmt.setString(4, rb.getId());
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("error on updateReportbug: "+e.toString());
			return false;
		}
	}
	
	
	public boolean inputReport(Reportbug report) {
		try {
			this.Connect();
			String query = "INSERT INTO `report_bugs`(`rb_username`, `rb_report`, `rb_links`) "
					+ "VALUES(?,?,?)";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, report.getUsername());
			stmt.setString(2, report.getReport());
			stmt.setString(3, report.getLinks());
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error in inputReport: "+e.toString());
			return false;
		}
	}
	
	public ArrayList<Reportbugstatus> getStatusreportlist(){
		try {
			this.Connect();
			ArrayList<Reportbugstatus> lists = new ArrayList<Reportbugstatus>();
			String query = "SELECT `frb_flag`, `frb_desc` FROM `flag_report_bugs`";
			stmt = conn.prepareStatement(query);
			
			rs = stmt.executeQuery();
			while (rs.next()) {
				lists.add(new Reportbugstatus(rs.getString("frb_flag"),rs.getString("frb_desc")));
			}
			
			this.closeConn();
			return lists;
		} catch (Exception e) {
			System.out.println("error in getStatusreportlist"+ e.toString());
			return null;
		}
	}
	
	public Reportbug getSingleRow(String id) {
		try {
			this.Connect();
			Reportbug rb = new Reportbug();
			String query = "SELECT `rb_id`, `rb_username`, `rb_report`, `rb_links`, `rb_status`, `rb_timestamp` "
					+ "FROM `report_bugs` "
					+ "WHERE `rb_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, id);
			
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				rb.setUsername(rs.getString("rb_username"));
				rb.setReport(rs.getString("rb_report"));
				rb.setLinks(rs.getString("rb_links"));
				rb.setStatus(rs.getInt("rb_status"));
				rb.setStempelwaktu(rs.getString("rb_timestamp"));
			}
			
			this.closeConn();
			return rb;
		} catch (Exception e) {
			System.out.println("error in getSingleRow: "+ e.toString());
			return null;
		}
	}
	
	public ArrayList<Reportbug> listtable(){
		try {
			this.Connect();
			ArrayList<Reportbug> mylist = new ArrayList<Reportbug>();
			String query = "SELECT `rb_id`, `rb_username`,`user`.`usr_nama` AS `nama`, `rb_report`, `rb_links`, `rb_status`,`flag_report_bugs`.`frb_desc` AS `desc`,`rb_timestamp` \n" + 
					"FROM `report_bugs` \n" + 
					"INNER JOIN `user` ON `user`.`usr_username`=`report_bugs`.`rb_username`\n" + 
					"INNER JOIN `flag_report_bugs` ON `flag_report_bugs`.`frb_flag`=`report_bugs`.`rb_status`";
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				Reportbug report = new Reportbug();
				report.setId(rs.getString("rb_id"));
				report.setUsername(rs.getString("nama"));
				report.setReport(rs.getString("rb_report"));
				report.setLinks(rs.getString("rb_links"));
				report.setStatusDesc(rs.getString("desc"));
				report.setStempelwaktu(rs.getString("rb_timestamp"));
				mylist.add(report);
			}
			
			this.closeConn();
			return mylist;
		} catch (Exception e) {
			System.out.println("error on listtable: "+ e.toString());
			return null;
		}
	}
}
