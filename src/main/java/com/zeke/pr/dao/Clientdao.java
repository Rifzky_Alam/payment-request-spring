package com.zeke.pr.dao;

import java.util.ArrayList;

import com.zeke.beans.PaymentRequest;
import com.zeke.beans.PrItem;
import com.zeke.login.dao.MySqlDbase;

public class Clientdao extends MySqlDbase{
	
	public boolean editItemrequest(PrItem item) {
		try {
			this.Connect();
			String query = "UPDATE `pr_items` " + 
					"SET `pri_itm_name`=?, `pri_price`=?,`pri_qty`=?, `pri_status`=?, `pri_ket`=? " + 
					"WHERE `pri_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, item.getItemname());
			stmt.setString(2, String.valueOf(item.getPrice()));
			stmt.setString(3, String.valueOf(item.getQty()));
			stmt.setString(4, String.valueOf(item.getStatus()));
			stmt.setString(5, item.getKeterangan());
			stmt.setString(6, String.valueOf(item.getId()));
			
			stmt.executeUpdate();
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error on editItemrequest: " + e.toString());
			return false;
		}
	}
	
	public boolean deleteItemrequest(PrItem item) {
		try {
			this.Connect();
			String query = "DELETE FROM `pr_items` WHERE `pri_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, String.valueOf(item.getId()));
			
			stmt.executeUpdate();
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error in model deleteitemrequest: " + e.toString());
			return false;
		}
	}
	
	
	public int inputRequestbaru(PaymentRequest pr) {
		try {
			this.Connect();
			String query = "INSERT INTO pr_lists (prl_requestor,prl_due,prl_status, prl_shop_name, prl_shop_contact) VALUES "
					+ "(?,?,?,?,?)";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, pr.getRequestor());
			stmt.setString(2, pr.getTgl());
			stmt.setString(3, "1");
			stmt.setString(4, pr.getShopname());
			stmt.setString(5, pr.getShopcontact());
			
			stmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}
	
	public int inputItemBaru(PrItem newitem) {
		try {
			this.Connect();
			String query = "INSERT INTO pr_items (pri_source, pri_itm_name, pri_price, pri_qty, pri_status, pri_ket) VALUES "
					+ "(?,?,?,?,?,?)";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, newitem.getSrc());
			stmt.setString(2, newitem.getItemname());
			stmt.setString(3, Integer.toString(newitem.getPrice()));
			stmt.setString(4, Integer.toString(newitem.getQty()));
			stmt.setString(5, Integer.toString(newitem.getStatus()));
			stmt.setString(6, newitem.getKeterangan());
			
			stmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			System.out.println(e.toString());
			return 0;
		}
	}
	
	public boolean removeitems(PaymentRequest pr) {
		try {
			this.Connect();
			String query;
			query = "DELETE FROM `pr_items` WHERE `pri_source`=?;";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, pr.getId());
			
			stmt.executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}
	}
	
	public boolean deleterow(PaymentRequest pr) {
		try {
			this.Connect();
			String query;
			query = "DELETE FROM `pr_lists` WHERE `prl_id`=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, pr.getId());
			
			stmt.executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}
	}
	
	public ArrayList<PaymentRequest> tabeldasbor(String username) {
		try {
			ArrayList<PaymentRequest> tabel = new ArrayList<PaymentRequest>();
			
			this.Connect();
			String query = "SELECT `prl_id`, `usr_nama`, `prl_requestor`,`fpl_desc`, `prl_status`," + 
					"(" + 
					"	SELECT GROUP_CONCAT(`pr_items`.`pri_itm_name` SEPARATOR '#') " + 
					"    FROM `pr_items`" + 
					"    WHERE `pr_items`.`pri_source`=`prl_id` AND `pr_items`.`pri_status`='1'" + 
					") AS items, "
					+ "("
					+ "SELECT SUM(`pri_price` * `pri_qty`) "
					+ "FROM `pr_items` "
					+ "WHERE `pr_items`.`pri_source`=`prl_id` AND `pr_items`.`pri_status`='1'"
					+ ") AS `total` " + 
					"FROM `pr_lists` " + 
					"INNER JOIN `flag_pr_list` " + 
					"ON `flag_pr_list`.`fpl_id` = `pr_lists`.`prl_status` "
					+ "INNER JOIN `user` ON `user`.`usr_username`=`pr_lists`.`prl_requestor`"
					+ "WHERE prl_requestor=? ORDER BY `pr_lists`.`prl_created_at` DESC";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, username);
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				tabel.add(
						new PaymentRequest(
								rs.getString("prl_id"),
								rs.getString("usr_nama"),
								PaymentRequest.getItemnames(rs.getString("items")),
								Integer.parseInt(PaymentRequest.checkTotal(rs.getString("total"))),
								rs.getString("fpl_desc"),
								Integer.parseInt(rs.getString("prl_status"))
						));
//				System.out.println(rs.getString("prl_id") + " "+rs.getString("prl_requestor"));
			}
			
			return tabel;
		} catch (Exception e) {
			System.out.println("Error database dasbor: " + e.toString());
			return null;
		}
	}
}
