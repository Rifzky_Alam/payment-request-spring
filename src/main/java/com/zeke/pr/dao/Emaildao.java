package com.zeke.pr.dao;

import java.util.ArrayList;

import com.zeke.beans.Emailbean;
import com.zeke.login.dao.MySqlDbase;

public class Emaildao extends MySqlDbase{
	
	public boolean insertnewEmail(Emailbean bean) {
		try {
			this.Connect();
			String query = "INSERT INTO `registered_email`(`re_account`, `re_password`, `re_host`, `re_port`, `re_status`) "
					+ "VALUES(?,?,?,?,?)";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, bean.getAccount());
			stmt.setString(2, bean.getPassword());
			stmt.setString(3, bean.getHostname());
			stmt.setInt(4, bean.getPortnumber());
			stmt.setString(5, "1");
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error in insert email: " + e.toString());
			return false;
		}
	}
	
	public boolean updateDataEmail(Emailbean bean) {
		try {
			this.Connect();
			String query = "UPDATE `registered_email` "
					+ "SET `re_account`=?,"
					+ "`re_password`=?,"
					+ "`re_host`=?,"
					+ "`re_port`=? WHERE `re_status`='1'";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, bean.getAccount());
			stmt.setString(2, bean.getPassword());
			stmt.setString(3, bean.getHostname());
			stmt.setInt(4, bean.getPortnumber());
			
			stmt.executeUpdate();
			
			this.closeConn();
			return true;
		} catch (Exception e) {
			System.out.println("Error in updateDataEmail:"+ e.toString());
			return false;
		}
	}
	
	
	public String countTotalAvailableEmail() {
		try {
			this.Connect();
			String hasil = "";
			String query = "SELECT COUNT(`re_account`) AS `jumlah` FROM `registered_email` WHERE `re_status`='1'";
			stmt = conn.prepareStatement(query);
			
			rs = stmt.executeQuery();
			while(rs.next()) {
				hasil = rs.getString("jumlah");
			}
			this.closeConn();
			return hasil;
		} catch (Exception e) {
			System.out.println("Error in countTotalAvailableEmail: " + e.toString());
			return "";
		}
	}
	
	public Emailbean getConfigValues() {
		try {
			this.Connect();
			Emailbean email = new Emailbean();
			String query = "SELECT `re_row_no`, `re_account`, `re_password`, `re_host`, `re_port`, `re_status` "
					+ "FROM `registered_email` "
					+ "WHERE `re_status`='1'";
			stmt = conn.prepareStatement(query);
			
			rs=stmt.executeQuery();
			while(rs.next()) {
				email.setAccount(rs.getString("re_account"));
				email.setPassword(rs.getString("re_password"));
				email.setPortnumber(rs.getInt("re_port"));
				email.setSendfrom(rs.getString("re_account"));
				email.setHostname(rs.getString("re_host"));
				
			}
			
			this.closeConn();
			return email;
		} catch (Exception e) {
			System.out.println("Error in getting values of email: "+ e.toString());
			return null;
		}
	}
	
	public ArrayList<Emailbean> getLists(){
		try {
			ArrayList<Emailbean> emailBeans = new ArrayList<Emailbean>();
			this.Connect();
			String query = "SELECT `re_row_no`, `re_account`, `re_password`, `re_host`, `re_port`, `re_status` "
					+ "FROM `registered_email`";
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Emailbean bean = new Emailbean();
				bean.setAccount(rs.getString("re_account"));
				bean.setHostname(rs.getString("re_host"));
				bean.setRownumber(rs.getString("re_row_no"));
				bean.setPortnumber(rs.getInt("re_port"));
				bean.setStatus(rs.getString("re_status"));
				emailBeans.add(bean);
			}
			this.closeConn();
			return emailBeans;
		} catch (Exception e) {
			System.out.println("Error on getLists: " + e.toString());
			return null;
		}
	}
	
}
