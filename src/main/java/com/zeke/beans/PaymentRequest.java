package com.zeke.beans;

import java.util.ArrayList;

public class PaymentRequest {
	private String id;
	private String labelPr;
	private String requestor;
	private String shopname;
	private String shopcontact;
	private ArrayList<PrItem> items;
	private String singleitem;
	private int totalvalues;
	private String tgl;
	private String status;
	private int flag;
	private String itimestamp;
	private String adminappv;
	private String financeappv;
	private String directorappv;
	private String notes;
	
	public PaymentRequest(String aidi, String req, ArrayList<PrItem> itm, int total, String admin, String finance, String director, String sts, int flg) {
		this.id = aidi;
		this.labelPr = this.createLabelpr(aidi);
		this.requestor = req;
		this.items = itm;
		this.totalvalues = total;
		this.adminappv = admin;
		this.financeappv = finance;
		this.directorappv = director;
		this.status = sts;
		this.flag = flg;
	}
	
	public PaymentRequest(String aidi, String req, ArrayList<PrItem> itm, int total, String sts, int flg) {
		this.id = aidi;
		this.labelPr = this.createLabelpr(aidi);
		this.requestor = req;
		this.items = itm;
		this.totalvalues = total;
		this.status = sts;
		this.flag = flg;
	}
	
	public PaymentRequest() {
		
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
		this.labelPr = this.createLabelpr(id);
	}
	public String getRequestor() {
		return requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
	
	public ArrayList<PrItem> getItems() {
		return items;
	}
	public void setItems(ArrayList<PrItem> items) {
		this.items = items;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	
	public static ArrayList<PrItem> getItemnames(String implstr){
		if (implstr==null) {
			return new ArrayList<PrItem>();
		} else {
			ArrayList<PrItem> hasil = new ArrayList<PrItem>();
			String [] items = implstr.split("#");
			for (String string : items) {
				hasil.add(new PrItem(string));
			}
			return hasil;
		}
		
	}

	public static String checkTotal(String str) {
		if (str==null) {
			return "0";
		}else {
			return str;
		}
	}
	
	public int getTotalvalues() {
		return totalvalues;
	}

	public void setTotalvalues(int totalvalues) {
		this.totalvalues = totalvalues;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	public String getItimestamp() {
		return itimestamp;
	}

	public void setItimestamp(String itimestamp) {
		this.itimestamp = itimestamp;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		if (shopname==null) {
			this.shopname = "";
		} else {
			this.shopname = shopname;
		}
		
	}

	public String getShopcontact() {
		return shopcontact;
	}

	public void setShopcontact(String shopcontact) {
		if (shopcontact==null) {
			this.shopcontact = "";
		} else {
			this.shopcontact = shopcontact;
		}
		
	}

	public String getAdminappv() {
		return adminappv;
	}

	public void setAdminappv(String adminappv) {
		if (adminappv==null) {
			this.adminappv = "";
		} else {
			this.adminappv = adminappv;
		}
		
	}

	public String getFinanceappv() {
		return financeappv;
	}

	public void setFinanceappv(String financeappv) {
		if (financeappv==null) {
			this.financeappv = "";
		} else {
			this.financeappv = financeappv;
		}
		
	}

	public String getDirectorappv() {
		return directorappv;
	}

	public void setDirectorappv(String directorappv) {
		if (directorappv==null) {
			this.directorappv = "";
		} else {
			this.directorappv = directorappv;
		}
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getLabelPr() {
		return labelPr;
	}

	public void setLabelPr(String labelPr) {
		this.labelPr = labelPr;
	}
	
	private String createLabelpr(String id) {
		int aidi = Integer.parseInt(id);
		String hasil = "";
		if (aidi<10) {
			hasil = "#000000"+String.valueOf(aidi);
		}else if(aidi>=10 && aidi < 100) {
			hasil = "#00000"+String.valueOf(aidi);
		}else if(aidi >= 100 && aidi < 1000) {
			hasil = "#0000"+String.valueOf(aidi);
		}else if(aidi>=1000 && aidi < 10000) {
			hasil = "#000"+String.valueOf(aidi);
		}else if(aidi>=10000 && aidi < 100000) {
			hasil = "#00"+String.valueOf(aidi);
		}else if(aidi>=100000 && aidi < 1000000) {
			hasil = "#0"+String.valueOf(aidi);
		}else if(aidi>=100000 && aidi < 1000000) {
			hasil = "#"+String.valueOf(aidi);
		}else {
			hasil = "#OutOfNumber!";
		}

		return hasil;
	}

	public String getSingleitem() {
		return singleitem;
	}

	public void setSingleitem(String singleitem) {
		this.singleitem = singleitem;
	}
	
}
