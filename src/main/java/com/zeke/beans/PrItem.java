package com.zeke.beans;

public class PrItem {
	
	private int id;
	private String src;
	private String itemname;
	private int price;
	private int qty;
	private int status;
	private String keterangan;
	
	public PrItem() {
		
	}
	
	public PrItem(int aidi, String source, String itmname, int harga, int jumlah) {
		this.id = aidi;
		this.src = source;
		this.itemname = itmname;
		this.price = harga;
		this.qty = jumlah;
	}
	
	public PrItem(String itmname) {
		this.itemname = itmname;
	}
	
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		if (keterangan==null) {
			this.keterangan = "";
		} else {
			this.keterangan = keterangan;
		}
		
	}
	
	
}
