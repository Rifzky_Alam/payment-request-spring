package com.zeke.beans;

public class Roleuser {
	private String id;
	private String desc;
	
	public Roleuser() {
		
	}
	
	public Roleuser(String idz, String desz) {
		this.id= idz;
		this.desc = desz;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
