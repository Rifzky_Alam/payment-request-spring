package com.zeke.beans;

import java.util.ArrayList;

public class Reportbug {
	private String id;
	private String username;
	private String report;
	private String links;
	private int status;
	private String statusDesc;
	private String stempelwaktu;
	private ArrayList<Reportbugstatus> listStatus;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getStempelwaktu() {
		return stempelwaktu;
	}
	public void setStempelwaktu(String stempelwaktu) {
		this.stempelwaktu = stempelwaktu;
	}
	public ArrayList<Reportbugstatus> getListStatus() {
		return listStatus;
	}
	public void setListStatus(ArrayList<Reportbugstatus> listStatus) {
		this.listStatus = listStatus;
	}
	
}
