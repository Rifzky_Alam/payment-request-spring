package com.zeke.beans;

public class Userstatus {
	private String id;
	private String desc;
	
	public Userstatus(){
		
	}
	
	public Userstatus(String idz, String descz) {
		this.id = idz;
		this.desc = descz;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
