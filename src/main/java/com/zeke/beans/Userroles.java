package com.zeke.beans;

public class Userroles {
	private String id;
	private String rolename;
	
	public Userroles(){
		
	}
	
	public Userroles(String aidi, String irolename) {
		this.id = aidi;
		this.rolename = irolename;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	
}
