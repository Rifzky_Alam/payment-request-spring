package com.zeke.beans;

public class Paymentrequeststatus {
	private String id;
	private String desc;
	
	public Paymentrequeststatus() {
		
	}
	public Paymentrequeststatus(String aidi, String description) {
		this.id = aidi;
		this.desc = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
