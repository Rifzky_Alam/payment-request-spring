package com.zeke.beans;

import java.util.ArrayList;

public class User {
	private String username;
	private String password;
	private String name;
	private String email;
	private String irole;
	private ArrayList<Roleuser> rolelists;
	private String status;
	private ArrayList<Userstatus> userstatus;
	
	public User() {
		
	}
	
	public User(String username, String pass) {
		this.username = username;
		this.password = pass;
	}
	
	public User(String username, String pass,String nama,String email, String role) {
		this.username = username;
		this.password = pass;
		this.name = nama;
		this.email = email;
		this.irole = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIrole() {
		return irole;
	}

	public void setIrole(String irole) {
		this.irole = irole;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<Userstatus> getUserstatus() {
		return userstatus;
	}

	public void setUserstatus(ArrayList<Userstatus> userstatus) {
		this.userstatus = userstatus;
	}

	public ArrayList<Roleuser> getRolelists() {
		return rolelists;
	}

	public void setRolelists(ArrayList<Roleuser> rolelists) {
		this.rolelists = rolelists;
	}
	
	
	
}
